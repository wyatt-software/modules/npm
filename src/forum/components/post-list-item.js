// This is a base component. add `usePostListitem(props)` to the child component's `setup()` method.
const MAX_CHARS = 100

import { ref, computed, onUnmounted } from 'vue'
import { useStore } from 'vuex'
import { StringHelper, useMarkdown, useMoment } from '../../core'

export function usePostListitem(props) {
  const store = useStore()
  const markdown = useMarkdown()
  const moment = useMoment()

  const postCreatedText = ref('')
  const postCreatedTooltip = ref('')
  const interval = ref(undefined)

  const isOp = computed(() => {
    return props.post.userId === props.opUserId
  })

  const isSummary = computed(() => {
    return props.view !== 'list'
  })

  const size = computed(() => {
    return isSummary.value ? 'md' : ''
  })

  const username = computed(() => {
    return props.post.user ? props.post.user.username : ''
  })

  const isModerator = computed(() => {
    return props.post.user ? props.post.user.moderator : false
  })

  const location = computed(() => {
    return props.post.user ? props.post.user.location : ''
  })

  const joined = computed(() => {
    return props.post.user ? props.post.user.created : moment()
  })

  const postCount = computed(() => {
    return props.post.user ? props.post.user.postCount : 0
  })

  const userTooltip = computed(() => {
    return `Joined ${moment(joined.value).format('DD-MMM YYYY')}\n${StringHelper.noun(
      postCount.value,
      'post',
      'posts'
    )}`
  })

  const userRoute = computed(() => {
    return props.post.user ? { name: 'identity-user-profile', params: { username: props.post.user.username } } : null
  })

  const topicRoute = computed(() => {
    return props.alias ? { name: 'forum-topic', params: { alias: props.alias } } : null
  })

  const postRoute = computed(() => {
    return props.alias && props.post.id
      ? { name: 'forum-topic', params: { alias: props.alias, postId: props.post.id }, hash: `#post${props.post.id}` }
      : null
  })

  const editedByTooltip = computed(() => {
    return props.post.editedByUser
      ? `Edited by ${props.post.editedByUser.username} on ${moment(props.post.editedDate).format('llll')}`
      : ''
  })

  const showSignature = computed(() => {
    return store.state.forum.persisted.showSignatures && !isSummary.value
  })

  const hasSignature = computed(() => {
    return props.post.user ? props.post.user.signature.length > 0 : false
  })

  const formattedSignature = computed(() => {
    return props.post.user ? markdown.toHtml(props.post.user.signature, store.state.forum.persisted.showEmojis) : ''
  })

  const formattedContent = computed(() => {
    return convertImageLinksToLightbox(markdown.toHtml(props.post.content, store.state.forum.persisted.showEmojis))
  })

  const summaryContent = computed(() => {
    return isSummary.value ? StringHelper.summary(formattedContent.value, MAX_CHARS) : formattedContent.value
  })

  const isPostOwner = computed(() => {
    return (
      store.state.identity.session.userId && props.post && store.state.identity.session.userId === props.post.userId
    )
  })

  const canEditThisPost = computed(() => {
    return isPostOwner.value || store.state.identity.session.roles.includes('Moderator')
  })

  const editPostRoute = computed(() => {
    return { name: 'forum-post-edit', params: { postId: props.post.id } }
  })

  const updateRelativeMomentText = () => {
    const postCreatedTextRelative = moment(props.post.created).fromNow()
    const postCreatedTextAbsolute = moment(props.post.created).format('llll')

    const relativeDatetimes = store.state.core.persisted.relativeDatetimes
    postCreatedText.value = relativeDatetimes ? postCreatedTextRelative : postCreatedTextAbsolute
    postCreatedTooltip.value = relativeDatetimes ? postCreatedTextAbsolute : postCreatedTextRelative
  }

  const convertImageLinksToLightbox = (html) => {
    // Adds the 'data-fancybox' and 'data-caption' attributes to any image links within the HTML.
    const div = document.createElement('div')
    div.innerHTML = html
    div.querySelectorAll('a img').forEach((img) => {
      const a = img.closest('a')
      a.setAttribute('data-fancybox', `post_${props.post.id}`)
      a.setAttribute('class', 'thumb')
      a.setAttribute('data-caption', img.alt)
    })
    return div.innerHTML
  }

  onUnmounted(() => {
    clearInterval(interval.value)
  })

  updateRelativeMomentText()
  interval.value = setInterval(function () {
    updateRelativeMomentText()
  }, 1000)

  return {
    postCreatedText,
    postCreatedTooltip,
    isOp,
    isSummary,
    size,
    username,
    isModerator,
    location,
    joined,
    postCount,
    userTooltip,
    userRoute,
    topicRoute,
    postRoute,
    editedByTooltip,
    showSignature,
    hasSignature,
    formattedSignature,
    formattedContent,
    summaryContent,
    isPostOwner,
    canEditThisPost,
    editPostRoute,
  }
}
