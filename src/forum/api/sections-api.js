import { UrlHelper } from '../../core'

export default class SectionsApi {
  constructor(api, auth) {
    this.api = api
    this.auth = auth
    this.controllerBase = `${this.api.baseUrl}forum/sections`
  }

  async fetchAsync(params) {
    // const params = {
    //   categoryId : 123,
    //   expand     : '' // [all|category|lastpost|lastpost.topic|lastpost.user]
    //   offset     : 0,
    //   limit      : 20,
    // }

    const url = UrlHelper.getUrl(this.controllerBase, params)
    return await this.api.http.get(url)
  }

  async createAsync(request) {
    // const request = {
    //   categoryId   : 123,
    //   codeName     : 'general',
    //   displayName  : 'General',
    //   description  : 'Stuff we can\'t categories',
    //   classified   : false,
    // }
    return await this.api.http.post(this.controllerBase, request, { headers: this.auth.getAuthHeaders() })
  }

  async readAsync(sectionIdOrCodeName, params) {
    // const params = {
    //   expand: '' // [all|category|lastpost|lastpost.topic|lastpost.user]
    // }
    const url = UrlHelper.getUrl(`${this.controllerBase}/${sectionIdOrCodeName}`, params)
    return await this.api.http.get(url)
  }

  async updateAsync(sectionIdOrCodeName, request) {
    // const request = {
    //   categoryId   : 123,
    //   codeName     : 'general',
    //   displayName  : 'General',
    //   description  : 'Stuff we can\'t categories',
    //   classified   : false,
    // }
    return await this.api.http.put(`${this.controllerBase}/${sectionIdOrCodeName}`, request, {
      headers: this.auth.getAuthHeaders(),
    })
  }

  async deleteAsync(sectionIdOrCodeName) {
    return await this.api.http.delete(`${this.controllerBase}/${sectionIdOrCodeName}`, {
      headers: this.auth.getAuthHeaders(),
    })
  }
}
