import { UrlHelper } from '../../core'

export default class PostsApi {
  constructor(api, auth) {
    this.api = api
    this.auth = auth
    this.controllerBase = `${this.api.baseUrl}forum/posts`
  }

  async fetchAsync(params) {
    // const params = {
    //   topic    : 'general', // id or alias
    //   expand   : '' // [all|topic|topic.section|user|editedbyuser]
    //   offset   : 0,
    //   limit    : 20,
    //   postId   : 0, // Specify instead of offset and limit to ensure the post is in the results.
    //   username : '',
    //   sort     : 'created', // prefix with minus sign to sort descending.
    // }
    const url = UrlHelper.getUrl(this.controllerBase, params)
    return await this.api.http.get(url)
  }

  async createAsync(request) {
    // const request = {
    //   topicId    : 123,
    //   content    : 'This can be formatted as **markdown**.',
    //   subscribe  : true,
    // }
    return await this.api.http.post(this.controllerBase, request, { headers: this.auth.getAuthHeaders() })
  }

  async readAsync(id, params) {
    // const params = {
    //   topic    : 'general', // id or alias
    //   expand   : '' // [all|topic|topic.section|user|editedbyuser]
    //   offset   : 0,
    //   limit    : 20,
    //   postId   : 0, // Specify instead of offset and limit to ensure the post is in the results.
    //   username : '',
    //   sort     : 'created', // prefix with minus sign to sort descending.
    // }
    const url = UrlHelper.getUrl(`${this.controllerBase}/${id}`, params)
    return await this.api.http.get(url)
  }

  async updateAsync(id, request) {
    // const request = {
    //   content: 'This can be formatted as **markdown**.',
    // }
    return await this.api.http.put(`${this.controllerBase}/${id}`, request, { headers: this.auth.getAuthHeaders() })
  }

  async deleteAsync(id) {
    return await this.api.http.delete(`${this.controllerBase}/${id}`, { headers: this.auth.getAuthHeaders() })
  }
}
