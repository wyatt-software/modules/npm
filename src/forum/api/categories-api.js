import { UrlHelper } from '../../core'

export default class CategoriesApi {
  constructor(api, auth) {
    this.api = api
    this.auth = auth
    this.controllerBase = `${this.api.baseUrl}forum/categories`
  }

  async fetchAsync(params) {
    // const params = {
    //   expand: '' // [all|sections|sections.lastpost|sections.lastpost.user|sections.lastpost.topic]
    // }
    const url = UrlHelper.getUrl(this.controllerBase, params)
    return await this.api.http.get(url)
  }

  async createAsync(request) {
    // const request = {
    //   displayName: 'whatever',
    // }
    return await this.api.http.post(this.controllerBase, request, { headers: this.auth.getAuthHeaders() })
  }

  async readAsync(id, params) {
    // const params = {
    //   expand: '' // [all|sections|sections.lastpost|sections.lastpost.user|sections.lastpost.topic]
    // }
    const url = UrlHelper.getUrl(`${this.controllerBase}/${id}`, params)
    return await this.api.http.get(url)
  }

  async updateAsync(id, request) {
    // const request = {
    //   displayName: 'whatever',
    // }
    return await this.api.http.put(`${this.controllerBase}/${id}`, request, { headers: this.auth.getAuthHeaders() })
  }

  async deleteAsync(id) {
    return await this.api.http.delete(`${this.controllerBase}/${id}`, { headers: this.auth.getAuthHeaders() })
  }
}
