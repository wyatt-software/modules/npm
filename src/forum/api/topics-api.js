import { UrlHelper } from '../../core'

export default class TopicsApi {
  constructor(api, auth) {
    this.api = api
    this.auth = auth
    this.controllerBase = `${this.api.baseUrl}forum/topics`
  }

  async fetchAsync(params) {
    // const params = {
    //   section    : 'general',
    //   expand     : '' // [all|section|user|lastpost|lastpost.user|image]
    //   offset     : 0,
    //   limit      : 20,
    //   username   : '',
    //   subscribed : null,
    // }
    const url = UrlHelper.getUrl(this.controllerBase, params)
    return await this.api.http.get(url, { headers: this.auth.getAuthHeaders() })
  }

  async createAsync(request) {
    // const request = {
    //   sectionId  : 123,
    //   imageId    : 0,
    //   title      : 'Test topic',
    //   subHeading : 'Amazing stuff',
    //   content    : 'This can be formatted as **markdown**.',
    //   imageAuto  : false,
    //   status     : 0, // Used when the section is a classifieds section.
    //   price      : 0,
    // }
    return await this.api.http.post(this.controllerBase, request, { headers: this.auth.getAuthHeaders() })
  }

  async readAsync(topicIdOrAlias, params) {
    // const params = {
    //   expand: '' // [all|section|user|lastpost|lastpost.user|image]
    // }
    const url = UrlHelper.getUrl(`${this.controllerBase}/${topicIdOrAlias}`, params)
    return await this.api.http.get(url, { headers: this.auth.getAuthHeaders() })
  }

  async updateAsync(topicIdOrAlias, request) {
    // const request = {
    //   sectionId  : 123,
    //   imageId    : 0,
    //   title      : 'Test topic',
    //   subHeading : 'Amazing stuff',
    //   imageAuto  : false,
    //   status     : 0, // Used when the section is a classifieds section.
    //   price      : 0,
    // }
    return await this.api.http.put(`${this.controllerBase}/${topicIdOrAlias}`, request, {
      headers: this.auth.getAuthHeaders(),
    })
  }

  async deleteAsync(topicIdOrAlias) {
    return await this.api.http.delete(`${this.controllerBase}/${topicIdOrAlias}`, {
      headers: this.auth.getAuthHeaders(),
    })
  }

  async subscribeAsync(topicIdOrAlias) {
    return await this.api.http.post(
      `${this.controllerBase}/${topicIdOrAlias}/actions/subscribe`,
      {},
      { headers: this.auth.getAuthHeaders() }
    )
  }

  async unsubscribeAsync(topicIdOrAlias) {
    return await this.api.http.post(
      `${this.controllerBase}/${topicIdOrAlias}/actions/unsubscribe`,
      {},
      { headers: this.auth.getAuthHeaders() }
    )
  }
}
