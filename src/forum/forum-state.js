import forumStatePersisted from './forum-state-persisted.js'

export default {
  namespaced: true,
  modules: {
    persisted: forumStatePersisted,
  },
}
