const state = () => ({
  showSignatures: true,
  showEmojis: true,
})

const getters = {
  showSignatures(state) {
    return state.showSignatures
  },
  showEmojis(state) {
    return state.showEmojis
  },
}

const mutations = {
  setShowSignatures(state, value) {
    state.showSignatures = value
  },
  setShowEmojis(state, value) {
    state.showEmojis = value
  },
}

const actions = {
  setShowSignatures({ commit }, value) {
    commit('setShowSignatures', value)
  },
  setShowEmojis({ commit }, value) {
    commit('setShowEmojis', value)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
