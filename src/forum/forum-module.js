import { useUserAddOn } from '../identity'
import UserPostsAddOn from './components/user-posts-add-on.vue'
import UserTopicsAddOn from './components/user-topics-add-on.vue'
import UserLocationAddOn from './components/user-location-add-on.vue'
import UserSignatureAddOn from './components/user-signature-add-on.vue'

export default {
  install() {},

  setup() {
    const userAddOn = useUserAddOn()

    userAddOn.profileTableRows.unshift(UserLocationAddOn)
    userAddOn.profileTableRows.push(UserTopicsAddOn)
    userAddOn.profileTableRows.push(UserPostsAddOn)
    userAddOn.profileTableRows.push(UserSignatureAddOn)

    userAddOn.formFields.push({
      field: {
        type: 'checkbox',
        name: 'emailNewPosts',
        label: "Email me when someone posts in a topic I'm subscribed to",
      },
      getter: (user) => user.preferences.emailNewPosts,
      setter: (request, value) => {
        request.preferences = request.preferences || {}
        request.preferences.emailNewPosts = value
        return request
      },
    })

    userAddOn.formFields.push({
      field: {
        type: 'text',
        name: 'location',
        label: 'Location',
        tooltip:
          "Visible to other users. Please at least write what country you're in. The city/town would be even better!",
        maxlength: 200,
        validate: 'required',
      },
      getter: (user) => user.properties.location,
      setter: (request, value) => {
        request.properties = request.properties || {}
        request.properties.location = value
        return request
      },
    })

    userAddOn.formFields.push({
      field: {
        type: 'markdown',
        name: 'signature',
        maxlength: 500,
      },
      getter: (user) => user.properties.signature,
      setter: (request, value) => {
        request.properties = request.properties || {}
        request.properties.signature = value
        return request
      },
    })

    userAddOn.filters.push({ text: 'Location', value: 'location' })

    userAddOn.listItemTableRows.unshift(UserLocationAddOn)
  },
}
