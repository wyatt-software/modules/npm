// This is a base component. Add `useMessageListItem(props)` to the child component's `setup` method.
import { ref, computed, onMounted, onUnmounted } from 'vue'
import { useStore } from 'vuex'
import { useMarkdown, useMoment } from '../../core'

export function useMessageListItem(props) {
  const store = useStore()
  const markdown = useMarkdown()
  const moment = useMoment()

  const sentText = ref('')
  const sentTooltip = ref('')
  const interval = ref(undefined)

  const formattedBody = computed(() => {
    return markdown.toHtml(props.message.body, true)
  })

  const updateRelativeMomentText = () => {
    const sentRelative = moment(props.message.created).fromNow()
    const sentAbsolute = moment(props.message.created).format('llll')

    const relativeDatetimes = store.state.core.persisted.relativeDatetimes
    sentText.value = relativeDatetimes ? sentRelative : sentAbsolute
    sentTooltip.value = relativeDatetimes ? sentAbsolute : sentRelative
  }

  onMounted(() => {
    updateRelativeMomentText()
    interval.value = setInterval(function () {
      updateRelativeMomentText()
    }, 1000)
  })

  onUnmounted(() => {
    clearInterval(interval.value)
  })

  return {
    message: props.message,
    moment,
    sentText,
    sentTooltip,
    formattedBody,
  }
}
