const state = () => ({
  unreadMessageCount: '',
  usersListLink: false,
})

const getters = {
  unreadMessageCount(state) {
    return state.unreadMessageCount
  },
  usersListLink(state) {
    return state.usersListLink
  },
}

const mutations = {
  setUnreadMessageCount(state, value) {
    state.unreadMessageCount = value
  },
  setUsersListLink(state, value) {
    state.usersListLink = value
  },
}

const actions = {
  setUnreadMessageCount({ commit }, value) {
    commit('setUnreadMessageCount', value)
  },
  setUsersListLink({ commit }, value) {
    commit('setUsersListLink', value)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
