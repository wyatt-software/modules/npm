import { UrlHelper } from '../../core'

export default class MessagesApi {
  constructor(api, auth) {
    this.api = api
    this.auth = auth
    this.controllerBase = `${this.api.baseUrl}messaging/messages`
  }

  async fetchConversationsAsync(params) {
    // const params = {
    //   expand  : '' // [all|user]
    //   offset  : 0,
    //   limit   : 20
    // }
    const url = UrlHelper.getUrl(this.controllerBase, params)
    return await this.api.http.get(url, { headers: this.auth.getAuthHeaders() })
  }

  async fetchMessagesAsync(username, params) {
    // const params = {
    //   expand  : '' // [all|user]
    //   offset  : -1,
    //   limit   : 20
    // }
    const url = UrlHelper.getUrl(`${this.controllerBase}/${username}`, params)
    return await this.api.http.get(url, { headers: this.auth.getAuthHeaders() })
  }

  async createAsync(username, request) {
    // const request = {
    //   body  : 'Oh, **hi!**',
    // }
    return await this.api.http.post(`${this.controllerBase}/${username}`, request, {
      headers: this.auth.getAuthHeaders(),
    })
  }
}
