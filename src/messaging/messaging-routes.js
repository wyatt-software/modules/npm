import { RouteHelper } from '../core'

import MessagingMessagesOverviewView from './views/overview/overview-view.vue'
import MessagingMessagesConversationView from './views/conversation/conversation-view.vue'

const components = [MessagingMessagesOverviewView, MessagingMessagesConversationView]

const meta = [
  {
    id: null,
    sortOrder: 1,
    name: 'messaging-messages-overview',
    parent: 'identity-account',
    path: '/messages',
    title: 'Messages',
    menuItem: true,
    iconClass: 'bi bi-envelope-fill',
    component: 'MessagingMessagesOverviewView',
    redirect: null,
    requiresAuthentication: true,
    requiredRoles: '1',
  },
  {
    id: null,
    sortOrder: 1,
    name: 'messaging-messages-conversation',
    parent: 'messaging-messages-overview',
    path: '/messages/:username',
    title: 'Conversation',
    menuItem: false,
    iconClass: 'bi bi--fill',
    component: 'MessagingMessagesConversationView',
    redirect: null,
    requiresAuthentication: true,
    requiredRoles: '1',
  },
]

export default meta.map((x) => RouteHelper.metaToRoute(x, components))
