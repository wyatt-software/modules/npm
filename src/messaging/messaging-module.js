import { useStore } from 'vuex'
import { useUserAddOn } from '../identity'
import UserMessageButtonAddOn from './components/user-message-button-add-on.vue'

export default {
  install(_, options) {
    this.options = options
  },

  setup() {
    // Temporarily store the options in the module.
    const usersListLink = typeof this.options?.usersListLink !== 'undefined' ? this.options.usersListLink : false

    const store = useStore()
    const userAddOn = useUserAddOn()

    store.dispatch('messaging/setUsersListLink', usersListLink)

    userAddOn.profileTableRows.push(UserMessageButtonAddOn)

    userAddOn.formFields.push({
      field: {
        name: 'emailNewMessages',
        type: 'checkbox',
        label: 'Email me when I receive a new private message',
      },
      getter: (user) => user.preferences.emailNewMessages,
      setter: (request, value) => {
        request.preferences = request.preferences || {}
        request.preferences.emailNewMessages = value
        return request
      },
    })
  },
}
