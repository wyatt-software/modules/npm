import { inject } from 'vue'

const EventBusSymbol = Symbol()

export default {
  install(app, options) {
    // Must provide a `mitt` event bus object.
    // We provide it TO this event-bus-plugin so that we can also pass it to other plugins like the
    // confirm-prompt-plugin and signalr-plugin at construction, and not have to fiddle around with the setup() method
    // within those dependant plugins.
    app.provide(EventBusSymbol, options.eventBus)
  },
}

export function useEventBus() {
  const eventBus = inject(EventBusSymbol)
  if (!eventBus) {
    throw "The event bus hasn't been installed in your app."
  }
  return eventBus
}
