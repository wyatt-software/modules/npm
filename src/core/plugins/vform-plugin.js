import { inject, ref, computed } from 'vue'
import StringHelper from '../lib/string-helper'
import ValidationHelper from '../lib/validation-helper.js'
import VFormCheckbox from '../components/vform/vform-checkbox.vue'
import VFormHidden from '../components/vform/vform-hidden.vue'
import VFormMarkdown from '../components/vform/vform-markdown/index.vue'
import VFormMutiSelect from '../components/vform/vform-multiselect.vue'
import VFormRadio from '../components/vform/vform-radio.vue'
import VFormRecaptcha from '../components/vform/vform-recaptcha.vue'
import VFormSelect from '../components/vform/vform-select.vue'
import VFormText from '../components/vform/vform-text.vue'
import VFormTextarea from '../components/vform/vform-textarea.vue'

const VFormSymbol = Symbol()

/**
 * A plugin for adding custom controls to the VForm component.
 */
class VFormPlugin {
  constructor(recaptchaPublicKey) {
    this.recaptchaPublicKey = recaptchaPublicKey

    // Dictionary of control components.
    // The key is used in the 'type' property in the fields array when creating a VForm.
    //
    // Register the VForm controls.
    // Can be added to by other modules.
    this.defaultComponent = 'text'
    this.components = {
      checkbox: VFormCheckbox,
      hidden: VFormHidden,
      markdown: VFormMarkdown,
      mutiselect: VFormMutiSelect,
      radio: VFormRadio,
      recaptcha: VFormRecaptcha,
      select: VFormSelect,
      text: VFormText,
      textarea: VFormTextarea,
    }

    // Dictionary of validation rules/functions.
    //
    // Can be added to by other modules.
    // Function arguments will be (value, args[], fields).
    this.validationRules = {
      required: (value) => {
        if (!value || !value.length) {
          return 'This field is required.'
        }
        return true
      },
      min: (value, [min]) => {
        if (value && value.length < min) {
          return `Must be at least ${min} characters.`
        }
        return true
      },
      email: (value) => {
        if (!value || !value.length) {
          return true
        }

        if (!/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(value)) {
          return 'Must be a valid email.'
        }
        return true
      },
      alphanum: (value) => {
        if (!/^[a-z0-9]+$/i.test(value)) {
          return 'Must contain only alpha-numeric characters (a-z, A-Z, and 0-9).'
        }
        return true
      },
      confirmed: (value, [target], fields) => {
        const targetValue = fields.find((x) => x.name === target).value
        if (value !== targetValue) {
          return "Confirmed value doesn't match."
        }
        return true
      },
    }
  }

  /**
   * Set the values of a fields array with a form dictionary.
   * @param {Array} fields - A collection of fields to validate.
   * @param {*} form - An object with a property (value) for each field.
   * @returns The mutated fields.
   */
  set(fields, form) {
    Object.keys(form).forEach((key) => {
      fields.find((x) => x.name === key).value = form[key]
    })

    return JSON.parse(JSON.stringify(fields))
  }

  /**
   * Validates all fields, mutating each of their their 'errors' collections.
   * Each field's validate property is a pipe-separated list of rule definitions.
   * Each rule definition is a rule name, followed by an optional colon then comma-separated set of arguments.
   * Eg:
   *     - "alphanum"
   *     - "required|email"
   *     - "confirmed:password"
   * @function
   * @param {Array} fields - A collection of fields to validate.
   * @returns An object containing the overall result, and a collection of mutated fields (with individual errors set).
   */
  async validateAsync(fields) {
    let result = true
    let mutatedFields = JSON.parse(JSON.stringify(fields))
    for (const field of mutatedFields.filter((x) => x.validate && x.validate.length > 0)) {
      field.errors = []
      for (const def of field.validate.split('|')) {
        const defParts = def.split(':')
        const rule = defParts[0]
        const args = defParts.length > 1 ? defParts[1].split(',') : []
        const func = this.validationRules[rule]
        if (func) {
          const ruleResult = await func(field.value, args, fields)
          if (ruleResult !== true) {
            field.errors.push(ruleResult)
            result = false
          }
        }
      }
    }
    return { result, mutatedFields }
  }

  /**
   * Clears validation errors on a collection of fields.
   * @function
   * @param {Array} fields Fields to clear.
   * @returns The mutated fields.
   */
  clearValidation(fields) {
    let mutatedFields = JSON.parse(JSON.stringify(fields))
    for (const field of mutatedFields.filter((x) => x.validate && x.validate.length > 0)) {
      field.errors = []
    }
    return mutatedFields
  }

  /**
   * Gets any errors for the given field name.
   * @function
   * @param {object} modelState
   * @param {string} fieldName
   * @returns
   */
  serverErrors(modelState, fieldName) {
    if (!modelState) {
      return []
    }
    const property = 'request.' + StringHelper.pascalCase(fieldName)
    return modelState[property]
  }

  /**
   * Gets the first error if any.
   * @param {object} modelState
   * @param {string} fieldName
   * @returns
   */
  firstServerError(modelState, fieldName) {
    const errors = this.serverErrors(modelState, fieldName)
    return ValidationHelper.arrayIsEmpty(errors) ? '' : errors[0]
  }
}

export default {
  install(app, options) {
    const recaptchaPublicKey = options?.recaptchaPublicKey || ''
    app.provide(VFormSymbol, new VFormPlugin(recaptchaPublicKey))
  },
}

export function useVForm() {
  const vform = inject(VFormSymbol)
  if (!vform) {
    throw "The VForm plugin hasn't been installed in your app."
  }
  return vform
}

/**
 * Use instead of Vue 2 "extends". Call in the setup() of child controls.
 * @function
 * @param {Object} props
 * @param {Object} context
 * @returns computed values.
 */
export function useVFormControl(props, context) {
  const vform = useVForm()

  const fieldValue = ref(props.field.value)

  const hasError = computed(() => {
    return props.field.errors && props.field.errors.length > 0
  })

  const error = computed(() => {
    return hasError.value ? props.field.errors[0] : firstServerError.value
  })

  const firstServerError = computed(() => {
    return vform.firstServerError(props.modelState, props.field.name)
  })

  const validationName = computed(() => {
    return props.field.label || props.field.placeholder
  })

  const isRequired = computed(() => {
    return props.field.validate && props.field.validate.split
      ? props.field.validate.split('|').includes('required')
      : false
  })

  const onButton = () => {
    context.emit('button', props.field)
  }

  const onSubmit = () => {
    context.emit('submit')
  }

  const onInput = (e) => {
    const mutatedField = JSON.parse(JSON.stringify(props.field))
    mutatedField.value = e.target.value
    context.emit('input', e)
    context.emit('fieldInput', mutatedField)
  }

  return {
    fieldValue,
    hasError,
    error,
    firstServerError,
    validationName,
    isRequired,
    onButton,
    onSubmit,
    onInput,
  }
}
