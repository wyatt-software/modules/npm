import { inject } from 'vue'
import showdown from 'showdown'

const MarkdownSymbol = Symbol()

/**
 * A plugin for converting markdown to HTML.
 */
class MarkdownPlugin {
  constructor(storageBaseUrl) {
    // https://github.com/showdownjs/showdown
    // See the wiki for docs
    showdown.setFlavor('github')
    this.storageBaseUrl = storageBaseUrl
    this.converter = new showdown.Converter()
  }

  /**
   * Converts markdown to HTML.
   * @param {string} md - The markdown string to convert.
   * @param {boolean} emoji - A value indicating whether to convert github emoji markdown to unicode.
   * @returns An HTML representation of the markdown.
   */
  toHtml(md, emoji) {
    // substitute macros
    // TODO: Extract to separate method.
    // TODO: Include all macros.
    md = md ? md.replaceAll('{{StorageBaseUrl}}', this.storageBaseUrl) : ''

    this.converter.setOption('emoji', emoji)
    const html = this.converter.makeHtml(md)
    return html
  }
}

export default {
  install(app, options) {
    app.provide(MarkdownSymbol, new MarkdownPlugin(options.storageBaseUrl))
  },
}

export function useMarkdown() {
  const markdown = inject(MarkdownSymbol)
  if (!markdown) {
    throw "The markdown plugin hasn't been installed in your app."
  }
  return markdown
}
