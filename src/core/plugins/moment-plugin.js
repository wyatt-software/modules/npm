import { inject } from 'vue'
import moment from 'moment-timezone'

const MomentSymbol = Symbol()

/**
 * Use with `app.Use()` in your app setup to provide a global instance of the moment-timezone library.
 */
export default {
  install(app) {
    app.provide(MomentSymbol, moment)
  },
}

export function useMoment() {
  const moment = inject(MomentSymbol)
  if (!moment) {
    throw "The moment plugin hasn't been installed in your app."
  }
  return moment
}
