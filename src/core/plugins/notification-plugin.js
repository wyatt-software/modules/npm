import { inject } from 'vue'
import VueNotification from '@kyvg/vue3-notification'

const NotificationSymbol = Symbol()

/**
 * A plugin for showing notifications.
 */
class NotificationPlugin {
  constructor(app) {
    this.notify = app.config.globalProperties.$notify
  }

  info(text) {
    this.notify({
      type: 'info',
      text: `<i class="bi bi-info-circle-fill"></i> <strong>${text}</strong>`,
      duration: 3000,
    })
  }

  success(text) {
    this.notify({
      type: 'success',
      text: `<i class="bi bi-check-circle-fill"></i> <strong>${text}</strong>`,
      duration: 1500,
    })
  }

  warning(text) {
    this.notify({
      type: 'warn',
      text: `<i class="bi bi-exclamation-triangle-fill"></i> <strong>${text}</strong>`,
      duration: 3000,
    })
  }

  error(text, ex) {
    let message = ex?.response?.status
      ? `<br/><strong>${ex.response.status}: ${ex?.response?.statusText}</strong>.`
      : text
    if (ex?.response?.data?.message) {
      message = `${text}<br/>${ex.response.data.message}`
    }

    this.notify({
      type: 'error',
      title: 'Oh no!',
      text: `<i class="bi bi-bug-fill"></i> ${message}`,
      duration: 3000,
    })

    if (text) {
      console.error(text)
    }

    if (ex) {
      console.error(ex)
    }
  }
}

/**
 * A plugin for showing notifications.
 */
export default {
  install(app) {
    app.use(VueNotification)
    app.provide(NotificationSymbol, new NotificationPlugin(app))
  },
}

export function useNotification() {
  const notification = inject(NotificationSymbol)
  if (!notification) {
    throw "The notification plugin hasn't been installed in your app."
  }
  return notification
}
