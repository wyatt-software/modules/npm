import { inject } from 'vue'

const ConfirmPromptSymbol = Symbol()

/**
 * A plugin for showing a confirm prompt.
 */
class ConfirmPromptPlugin {
  constructor(eventBus) {
    // Must provide a `mitt` event bus object.
    this.eventBus = eventBus
  }

  /**
   * Shows the confirm prompt, then await a response.
   */
  show(options) {
    return new Promise((resolve, reject) => {
      options.text = options.text || 'Are you sure?'
      options.title = options.title || 'Are you sure?'
      options.resolve = resolve
      options.reject = reject
      this.eventBus.emit('confirm-prompt-show', options)
    })
  }
}

export default {
  install(app, options) {
    app.provide(ConfirmPromptSymbol, new ConfirmPromptPlugin(options.eventBus))
  },
}

export function useConfirmPrompt() {
  const confirmPrompt = inject(ConfirmPromptSymbol)
  if (!confirmPrompt) {
    throw "The confirm prompt hasn't been installed in your app."
  }
  return confirmPrompt
}
