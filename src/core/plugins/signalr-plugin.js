import { inject } from 'vue'
import { HubConnectionBuilder } from '@microsoft/signalr'

const SignalrSymbol = Symbol()

/**
 * A plugin for connecting to the core module's signalr hub.
 */
class SignalrPlugin {
  constructor(apiBaseUrl, eventBus) {
    this.apiBaseUrl = apiBaseUrl
    this.eventBus = eventBus // Must provide a `mitt` event bus object.
    this.connection = new HubConnectionBuilder().withUrl(`${apiBaseUrl}core/hub`).build()

    this.connection.on('output', (data) => {
      this.eventBus.emit('signalr-output', data)
    })

    this.connection.on('progress', (data) => {
      this.eventBus.emit('signalr-progress', data)
    })

    this.connection.on('complete', (data) => {
      this.eventBus.emit('signalr-complete', data)
    })

    this.connection.on('error', (data) => {
      this.eventBus.emit('signalr-error', data)
    })

    this.connection.onclose(() => this.start())

    this.start()
  }

  start() {
    this.connection.start()
  }
}

export default {
  install(app, options) {
    const apiBaseUrl = options.apiBaseUrl
    const eventBus = options.eventBus
    app.provide(SignalrSymbol, new SignalrPlugin(apiBaseUrl, eventBus))
  },
}

export function useSignalR() {
  const signalr = inject(SignalrSymbol)
  if (!signalr) {
    throw "The SignalR plugin hasn't been installed in your app."
  }
  return signalr
}
