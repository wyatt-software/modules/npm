import { inject } from 'vue'
import axios from 'axios'

const ApiSymbol = Symbol()

/**
 * A plugin (which is essentially just a wrapper) for providing the axios library.
 */
class ApiPlugin {
  constructor(apiBaseUrl, storageBaseUrl) {
    this.baseUrl = apiBaseUrl
    this.storageBaseUrl = storageBaseUrl
    this.http = axios
  }
}

export default {
  install(app, options) {
    const apiBaseUrl = options?.apiBaseUrl || 'http://localhost:5000/'
    const storageBaseUrl = options?.storageBaseUrl || 'http://localhost:5000/storage/'
    app.provide(ApiSymbol, new ApiPlugin(apiBaseUrl, storageBaseUrl))
  },
}

export function useApi() {
  const api = inject(ApiSymbol)
  if (!api) {
    throw "The API plugin hasn't been installed in your app."
  }
  return api
}
