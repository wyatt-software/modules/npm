import coreStatePersisted from './core-state-persisted.js'

const state = () => ({
  // View meta.
  title: '',
  iconClass: '',
  breadcrumbs: [],
  titleButtons: [],

  // VForm load/save state.
  // This is for loading and saving the state of a VForm, so that we can navigate away for things like image selection
  // (which isn't done via a popup etc to keep the UI mobile-friendly, and allow for URLs to update when on different
  // pages).
  vformState: null,
})

const getters = {
  // View meta.
  title(state) {
    return state.title
  },
  iconClass(state) {
    return state.iconClass
  },
  breadcrumbs(state) {
    return state.breadcrumbs
  },
  titleButtons(state) {
    return state.titleButtons
  },

  // VForm load/save state.
  vformState(state) {
    return state.vformState
  },
}

const mutations = {
  // View meta.
  setTitle(state, value) {
    state.title = value
  },
  setIconClass(state, value) {
    state.iconClass = value
  },
  setBreadcrumbs(state, value) {
    state.breadcrumbs = value
  },
  setTitleButtons(state, value) {
    state.titleButtons = value
  },
  clearViewMeta(state) {
    state.title = ''
    state.iconClass = ''
    state.breadcrumbs = []
    state.titleButtons = []
  },

  // VForm load/save state.
  setVformState(state, value) {
    state.vformState = value
  },
}

const actions = {
  // View meta.
  setTitle({ commit }, value) {
    commit('setTitle', value)
  },
  setIconClass({ commit }, value) {
    commit('setIconClass', value)
  },
  setBreadcrumbs({ commit }, value) {
    commit('setBreadcrumbs', value)
  },
  setTitleButtons({ commit }, value) {
    commit('setTitleButtons', value)
  },
  clearViewMeta({ commit }) {
    commit('clearViewMeta')
  },

  // VForm load/save state.
  setVformState({ commit }, value) {
    commit('setVformState', value)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
  modules: {
    persisted: coreStatePersisted,
  },
}
