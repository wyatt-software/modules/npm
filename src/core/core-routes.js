import RouteHelper from './lib/route-helper.js'

import CoreNotFoundView from './views/not-found-view.vue'

const components = [CoreNotFoundView]

const meta = [
  {
    id: null,
    sortOrder: 1,
    name: '404',
    parent: null,
    path: '/404',
    title: 'Not Found',
    menuItem: false,
    iconClass: '',
    component: 'CoreNotFoundView',
    redirect: null,
    requiresAuthentication: false,
    requiredRoles: '',
  },
]

export default meta.map((x) => RouteHelper.metaToRoute(x, components))
