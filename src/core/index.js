import CoreModule from './core-module.js'
import CoreRoutes from './core-routes.js'
import CoreState from './core-state.js'

import Constants from './constants.js'

import ConfirmPrompt from './components/confirm-prompt.vue'
import FancyAlert from './components/fancy-alert.vue'
import FixedFooter from './components/fixed-footer.vue'
import InfoTooltip from './components/info-tooltip.vue'
import LoadingSpinner from './components/loading-spinner.vue'
import ModalWindow from './components/modal-window.vue'
import PaginationNav from './components/pagination-nav.vue'
import ProgressBar from './components/progress-bar.vue'
import RequiredIcon from './components/required-icon.vue'
import VForm from './components/vform/index.vue'
import VTable from './components/vtable.vue'

import EmojiHelper from './lib/emojis/emojis-helper.js'
import ArrayExtensions from './lib/array-extensions.js'
import ObjectHelper from './lib/object-helper.js'
import RouteHelper from './lib/route-helper.js'
import StringHelper from './lib/string-helper.js'
import UrlHelper from './lib/url-helper.js'
import ValidationHelper from './lib/validation-helper.js'
import VideoHelper from './lib/video-helper.js'

import { useApi } from './plugins/api-plugin.js'
import { useConfirmPrompt } from './plugins/confirm-prompt-plugin.js'
import { useEventBus } from './plugins/event-bus-plugin.js'
import { useMarkdown } from './plugins/markdown-plugin.js'
import { useMoment } from './plugins/moment-plugin.js'
import { useNotification } from './plugins/notification-plugin.js'
import { useVForm, useVFormControl } from './plugins/vform-plugin.js'
import { useSignalR } from './plugins/signalr-plugin.js'

import CoreMenuView from './views/menu-view.vue'

export {
  CoreModule,
  CoreRoutes,
  CoreState,
  Constants,
  ConfirmPrompt,
  FancyAlert,
  FixedFooter,
  InfoTooltip,
  LoadingSpinner,
  ModalWindow,
  PaginationNav,
  ProgressBar,
  RequiredIcon,
  VForm,
  VTable,
  EmojiHelper,
  ArrayExtensions,
  ObjectHelper,
  RouteHelper,
  StringHelper,
  UrlHelper,
  ValidationHelper,
  VideoHelper,
  useApi,
  useConfirmPrompt,
  useEventBus,
  useMarkdown,
  useMoment,
  useNotification,
  useVForm,
  useVFormControl,
  useSignalR,
  CoreMenuView,
}
