export default class ObjectHelper {
  // Adapted from https://stackoverflow.com/questions/194846/is-there-any-kind-of-hash-code-function-in-javascript
  static getHash(obj) {
    const text = JSON.stringify(obj)
    let hash = 0
    for (let i = 0; i < text.length; i++) {
      const character = text.charCodeAt(i)
      hash = (hash << 5) - hash + character
      hash = hash & hash // Convert to 32bit integer
    }
    return hash
  }

  static equals(a, b) {
    const aStr = a ? JSON.stringify(a) : a
    const bStr = b ? JSON.stringify(b) : b
    return aStr === bStr
  }
}
