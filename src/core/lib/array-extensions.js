export default class ArrayExtensions {
  static register() {
    // Returns the result of merging this array with another.
    //
    // Adapted from Oriol's answer in
    // https://stackoverflow.com/questions/19480008/javascript-merging-objects-by-id
    //
    Array.prototype.mergeWith = function (a2) {
      const hash = new Map()
      this.concat(a2).forEach(function (obj) {
        hash.set(obj.id, Object.assign(hash.get(obj.id) || {}, obj))
      })
      const a3 = Array.from(hash.values())
      return a3
    }

    // Returns this array sorted by the given key
    //
    // Adapted from David Brainer's answer in
    // https://stackoverflow.com/questions/8837454/sort-array-of-objects-by-single-key-with-date-value
    //
    Array.prototype.sortByKey = function (key) {
      return this.sort(function (a, b) {
        const x = a[key]
        const y = b[key]
        return x < y ? -1 : x > y ? 1 : 0
      })
    }

    // Returns the sum of the given field.
    //
    // Adapted from @gruff-bunny's suggestion in
    // https://stackoverflow.com/questions/23247859/better-way-to-sum-a-property-value-in-an-array
    Array.prototype.sum = function (key) {
      return this.reduce(function (a, b) {
        return b[key] === null ? a : a + b[key]
      }, 0)
    }

    // Returns a set of unique values from this array.
    //
    // Adapted from Vlad Bezden's answer in
    // https://stackoverflow.com/questions/15125920/how-to-get-distinct-values-from-an-array-of-objects-in-javascript
    //
    Array.prototype.unique = function (key) {
      return [...new Set(this.map((item) => item[key]))]
    }

    // Returns true if this array contains all elements in array 'b'.
    //
    // Adapted from Eddie's answer in
    // https://stackoverflow.com/questions/51256750/check-if-an-javascript-array-contains-all-of-the-values-of-another-array
    //
    Array.prototype.containsArray = function (a2) {
      if (!a2 || a2.length === 0) {
        return true
      }
      return this.every((o) => a2.includes(o))
    }

    // Return the first element in the array, or undefined.
    Array.prototype.first = function () {
      return this.find(() => true)
    }
  }
}
