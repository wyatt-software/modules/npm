export default class RouteHelper {
  /**
   * @function
   * @param {object} meta - The metadata for the route.
   * @param {Array} components - Collection of components to search and map the component name to.
   * @returns An object that can be passed to the Vue Router's addRoute function.
   */
  static metaToRoute(meta, components) {
    const component = components.find((x) => x.name === meta.component)

    return {
      name: meta.name,
      path: meta.path,
      component: typeof component === 'object' ? component : null,
      redirect: meta.redirect,
      meta,
    }
  }

  /**
   * Get sub-views of the given id where .menuItem is true.
   * @param {object} router
   * @param {String} name of parent.
   * @returns A collection of routes.
   */
  static getSubMenu(router, name) {
    return router.getRoutes().filter((r) => r.meta.parent === name && r.meta.menuItem)
  }
}
