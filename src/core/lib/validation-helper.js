export default class ValidationHelper {
  // Returns true if this array is empty.
  //
  static arrayIsEmpty(array) {
    return array === undefined || array === null || array.length === 0
  }

  static getBoolean(value, defaultValue) {
    return typeof value === 'undefined' ? defaultValue : Boolean(value)
  }

  static getNumber(value, defaultValue) {
    return typeof value === 'undefined' ? defaultValue : Number(value)
  }

  static getInteger(value, defaultValue) {
    return typeof value === 'undefined' ? defaultValue : parseInt(Number(value))
  }

  static getFloat(value, defaultValue) {
    return typeof value === 'undefined' ? defaultValue : parseFloat(Number(value))
  }

  static getString(value, defaultValue) {
    return typeof value === 'undefined' ? defaultValue : String(value)
  }
}
