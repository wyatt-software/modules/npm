export default class StringHelper {
  // Adapted from https://stackoverflow.com/questions/2970525/converting-any-string-into-camel-case
  static camelCase(str) {
    return str
      .replace(/(?:^\w|[A-Z]|\b\w)/g, function (letter, index) {
        return index === 0 ? letter.toLowerCase() : letter.toUpperCase()
      })
      .replace(/\s+/g, '')
  }

  static crop(text, maxChars) {
    return text.length > maxChars ? text.substring(0, maxChars) + '...' : text
  }

  static insert(text, insertText, start, end) {
    if (end < start) {
      const temp = end
      end = start
      start = temp
    }
    const before = text.substring(0, start)
    const after = text.substring(end)
    return before + insertText + after
  }

  static defaultToEmpty(value, defaultValue) {
    return value === defaultValue ? '' : value
  }

  static emptyToDefault(value, defaultValue) {
    return typeof value === 'undefined' || value === '' ? defaultValue : value
  }

  // Taken from https://stackoverflow.com/questions/154059/how-can-i-check-for-an-empty-undefined-null-string-in-javascript
  static isString(str) {
    return !str || 0 === str.length
  }

  static noun(qty, singular, plural) {
    return qty === 1 ? `${qty} ${singular}` : `${qty} ${plural}`
  }

  // Taken from https://stackoverflow.com/questions/1026069/how-do-i-make-the-first-letter-of-a-string-uppercase-in-javascript
  static pascalCase(str) {
    return str.charAt(0).toUpperCase() + str.slice(1)
  }

  static stripHtml(html) {
    const div = document.createElement('div')
    div.innerHTML = html
    return div.textContent || div.innerText || ''
  }

  static summary(html, maxChars) {
    return this.crop(this.stripHtml(html), maxChars)
  }
}
