export default class UrlHelper {
  static qs(params) {
    return new URLSearchParams(Object.entries(params)).toString()
  }

  static getUrl(resource, params) {
    const queryString = this.qs(params)
    if (queryString) {
      return `${resource}?${queryString}`
    } else {
      return resource
    }
  }

  // Try to follow a router-link if there's a match.
  static routeTo(router, href, navigateIfNotFound) {
    const routes = router.getRoutes()
    const route = routes?.length > 0 ? routes.filter((r) => r.path === href)[0] : null
    if (route || (!route && !navigateIfNotFound)) {
      router.push(href)
    } else if (navigateIfNotFound) {
      window.location.href = href
    }
  }
}
