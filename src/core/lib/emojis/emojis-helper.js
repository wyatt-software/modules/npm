import emojiRegex from 'emoji-regex'

import Categories from './categories/index'

export default class EmojisHelper {
  static getCategories() {
    return Categories
  }

  static getAllEmojis() {
    return [].concat.apply(
      [],
      Categories.map((x) => x.emojis)
    )
  }

  static markdownToUnicode(text) {
    const allEmojis = this.getAllEmojis()
    return text.replace(/:.*?:/gi, function (match) {
      const emoji = allEmojis.find((x) => x.markdown === match)
      return emoji ? emoji.unicode : match
    })
  }

  static unicodeToMarkdown(text) {
    const er = emojiRegex()
    const allEmojis = this.getAllEmojis()

    let markdownWithEmojis = text
    let match

    while ((match = er.exec(markdownWithEmojis))) {
      const unicodeEmoji = match[0]
      const markdownEmoji = allEmojis.find((x) => x.unicode === unicodeEmoji).markdown
      markdownWithEmojis = markdownWithEmojis.replace(unicodeEmoji, markdownEmoji)
    }

    return markdownWithEmojis
  }
}
