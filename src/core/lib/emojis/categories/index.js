import SmileysPeople from './smileys-people'
import AnimalsNature from './animals-nature'
import FoodDrink from './food-drink'
import Activities from './activities'
import TravelPlaces from './travel-places'
import Objects from './objects'
import Symbols from './symbols'

export default [
  {
    icon: '😀',
    code: 'smileys-people',
    displayName: 'Smileys & People',
    emojis: SmileysPeople,
  },
  {
    icon: '🐶',
    code: 'animals-nature',
    displayName: 'Animals & Nature',
    emojis: AnimalsNature,
  },
  {
    icon: '🍎',
    code: 'food-drink',
    displayName: 'Food & Drink',
    emojis: FoodDrink,
  },
  {
    icon: '⚽️',
    code: 'activities',
    displayName: 'Activities',
    emojis: Activities,
  },
  {
    icon: '🚗',
    code: 'travel-places',
    displayName: 'Travel & Places',
    emojis: TravelPlaces,
  },
  {
    icon: '💡',
    code: 'objects',
    displayName: 'Objects',
    emojis: Objects,
  },
  {
    icon: '❤️',
    code: 'symbols',
    displayName: 'Symbols',
    emojis: Symbols,
  },
]
