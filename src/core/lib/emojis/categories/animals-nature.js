export default [
  { unicode: '🐶', markdown: ':dog:' },
  { unicode: '🐱', markdown: ':cat:' },
  { unicode: '🐭', markdown: ':mouse:' },
  { unicode: '🐹', markdown: ':hamster:' },
  { unicode: '🐰', markdown: ':rabbit:' },
  { unicode: '🦊', markdown: ':fox_face:' },
  { unicode: '🐻', markdown: ':bear:' },
  { unicode: '🐼', markdown: ':panda_face:' },
  { unicode: '🐨', markdown: ':koala:' },
  { unicode: '🐯', markdown: ':tiger:' },
  { unicode: '🦁', markdown: ':lion:' },
  { unicode: '🐮', markdown: ':cow:' },
  { unicode: '🐷', markdown: ':pig:' },
  { unicode: '🐽', markdown: ':pig_nose:' },
  { unicode: '🐸', markdown: ':frog:' },
  { unicode: '🐵', markdown: ':monkey_face:' },
  { unicode: '🙈', markdown: ':see_no_evil:' },
  { unicode: '🙉', markdown: ':hear_no_evil:' },
  { unicode: '🙊', markdown: ':speak_no_evil:' },
  { unicode: '🐒', markdown: ':monkey:' },
  { unicode: '🐔', markdown: ':chicken:' },
  { unicode: '🐧', markdown: ':penguin:' },
  { unicode: '🐦', markdown: ':bird:' },
  { unicode: '🐤', markdown: ':baby_chick:' },
  { unicode: '🐣', markdown: ':hatching_chick:' },
  { unicode: '🐥', markdown: ':hatched_chick:' },
  { unicode: '🦆', markdown: ':duck:' },
  { unicode: '🦅', markdown: ':eagle:' },
  { unicode: '🦉', markdown: ':owl:' },
  { unicode: '🦇', markdown: ':bat:' },
  { unicode: '🐺', markdown: ':wolf:' },
  { unicode: '🐗', markdown: ':boar:' },
  { unicode: '🐴', markdown: ':horse:' },
  { unicode: '🦄', markdown: ':unicorn:' },
  { unicode: '🐝', markdown: ':bee:' },
  { unicode: '🐛', markdown: ':bug:' },
  { unicode: '🦋', markdown: ':butterfly:' },
  { unicode: '🐌', markdown: ':snail:' },
  { unicode: '🐞', markdown: ':beetle:' },
  { unicode: '🕷', markdown: ':spider:' },
  { unicode: '🕸', markdown: ':spider_web:' },
  { unicode: '🦂', markdown: ':scorpion:' },
  { unicode: '🐢', markdown: ':turtle:' },
  { unicode: '🐍', markdown: ':snake:' },
  { unicode: '🦎', markdown: ':lizard:' },
  { unicode: '🐙', markdown: ':octopus:' },
  { unicode: '🦑', markdown: ':squid:' },
  { unicode: '🦐', markdown: ':shrimp:' },
  { unicode: '🦀', markdown: ':crab:' },
  { unicode: '🐡', markdown: ':blowfish:' },
  { unicode: '🐠', markdown: ':tropical_fish:' },
  { unicode: '🐟', markdown: ':fish:' },
  { unicode: '🐬', markdown: ':dolphin:' },
  { unicode: '🐳', markdown: ':whale:' },
  { unicode: '🐋', markdown: ':whale2:' },
  { unicode: '🦈', markdown: ':shark:' },
  { unicode: '🐊', markdown: ':crocodile:' },
  { unicode: '🐅', markdown: ':tiger2:' },
  { unicode: '🐆', markdown: ':leopard:' },
  { unicode: '🦍', markdown: ':gorilla:' },
  { unicode: '🐘', markdown: ':elephant:' },
  { unicode: '🦏', markdown: ':rhinoceros:' },
  { unicode: '🐪', markdown: ':dromedary_camel:' },
  { unicode: '🐫', markdown: ':camel:' },
  { unicode: '🐃', markdown: ':water_buffalo:' },
  { unicode: '🐂', markdown: ':ox:' },
  { unicode: '🐄', markdown: ':cow2:' },
  { unicode: '🐎', markdown: ':racehorse:' },
  { unicode: '🐖', markdown: ':pig2:' },
  { unicode: '🐏', markdown: ':ram:' },
  { unicode: '🐑', markdown: ':sheep:' },
  { unicode: '🐐', markdown: ':goat:' },
  { unicode: '🦌', markdown: ':deer:' },
  { unicode: '🐕', markdown: ':dog2:' },
  { unicode: '🐩', markdown: ':poodle:' },
  { unicode: '🐈', markdown: ':cat2:' },
  { unicode: '🐓', markdown: ':rooster:' },
  { unicode: '🦃', markdown: ':turkey:' },
  { unicode: '🕊', markdown: ':dove:' },
  { unicode: '🐇', markdown: ':rabbit2:' },
  { unicode: '🐁', markdown: ':mouse2:' },
  { unicode: '🐀', markdown: ':rat:' },
  { unicode: '🐿', markdown: ':chipmunk:' },
  { unicode: '🐾', markdown: ':feet:' },
  { unicode: '🐉', markdown: ':dragon:' },
  { unicode: '🐲', markdown: ':dragon_face:' },
  { unicode: '🌵', markdown: ':cactus:' },
  { unicode: '🎄', markdown: ':christmas_tree:' },
  { unicode: '🌲', markdown: ':evergreen_tree:' },
  { unicode: '🌳', markdown: ':deciduous_tree:' },
  { unicode: '🌴', markdown: ':palm_tree:' },
  { unicode: '🌱', markdown: ':seedling:' },
  { unicode: '🌿', markdown: ':herb:' },
  { unicode: '☘️', markdown: ':shamrock:' },
  { unicode: '🍀', markdown: ':four_leaf_clover:' },
  { unicode: '🎍', markdown: ':bamboo:' },
  { unicode: '🎋', markdown: ':tanabata_tree:' },
  { unicode: '🍃', markdown: ':leaves:' },
  { unicode: '🍂', markdown: ':fallen_leaf:' },
  { unicode: '🍁', markdown: ':maple_leaf:' },
  { unicode: '🍄', markdown: ':mushroom:' },
  { unicode: '🐚', markdown: ':shell:' },
  { unicode: '🌾', markdown: ':ear_of_rice:' },
  { unicode: '💐', markdown: ':bouquet:' },
  { unicode: '🌷', markdown: ':tulip:' },
  { unicode: '🌹', markdown: ':rose:' },
  { unicode: '🥀', markdown: ':wilted_flower:' },
  { unicode: '🌺', markdown: ':hibiscus:' },
  { unicode: '🌸', markdown: ':cherry_blossom:' },
  { unicode: '🌼', markdown: ':blossom:' },
  { unicode: '🌻', markdown: ':sunflower:' },
  { unicode: '🌞', markdown: ':sun_with_face:' },
  { unicode: '🌝', markdown: ':full_moon_with_face:' },
  { unicode: '🌛', markdown: ':first_quarter_moon_with_face:' },
  { unicode: '🌜', markdown: ':last_quarter_moon_with_face:' },
  { unicode: '🌚', markdown: ':new_moon_with_face:' },
  { unicode: '🌕', markdown: ':full_moon:' },
  { unicode: '🌖', markdown: ':waning_gibbous_moon:' },
  { unicode: '🌗', markdown: ':last_quarter_moon:' },
  { unicode: '🌘', markdown: ':waning_crescent_moon:' },
  { unicode: '🌑', markdown: ':new_moon:' },
  { unicode: '🌒', markdown: ':waxing_crescent_moon:' },
  { unicode: '🌓', markdown: ':first_quarter_moon:' },
  { unicode: '🌔', markdown: ':moon:' },
  { unicode: '🌙', markdown: ':crescent_moon:' },
  { unicode: '🌎', markdown: ':earth_americas:' },
  { unicode: '🌍', markdown: ':earth_africa:' },
  { unicode: '🌏', markdown: ':earth_asia:' },
  { unicode: '💫', markdown: ':dizzy:' },
  { unicode: '⭐️', markdown: ':star:' },
  { unicode: '🌟', markdown: ':star2:' },
  { unicode: '✨', markdown: ':sparkles:' },
  { unicode: '⚡️', markdown: ':zap:' },
  { unicode: '☄️', markdown: ':comet:' },
  { unicode: '💥', markdown: ':boom:' },
  { unicode: '🔥', markdown: ':fire:' },
  { unicode: '🌪', markdown: ':tornado:' },
  { unicode: '🌈', markdown: ':rainbow:' },
  { unicode: '☀️', markdown: ':sunny:' },
  { unicode: '🌤', markdown: ':sun_behind_small_cloud:' },
  { unicode: '⛅️', markdown: ':partly_sunny:' },
  { unicode: '🌥', markdown: ':sun_behind_large_cloud:' },
  { unicode: '☁️', markdown: ':cloud:' },
  { unicode: '🌦', markdown: ':sun_behind_rain_cloud:' },
  { unicode: '🌧', markdown: ':cloud_with_rain:' },
  { unicode: '⛈', markdown: ':cloud_with_lightning_and_rain:' },
  { unicode: '🌩', markdown: ':cloud_with_lightning:' },
  { unicode: '🌨', markdown: ':cloud_with_snow:' },
  { unicode: '❄️', markdown: ':snowflake:' },
  { unicode: '☃️', markdown: ':snowman_with_snow:' },
  { unicode: '⛄️', markdown: ':snowman:' },
  { unicode: '🌬', markdown: ':wind_face:' },
  { unicode: '💨', markdown: ':dash:' },
  { unicode: '💧', markdown: ':droplet:' },
  { unicode: '💦', markdown: ':sweat_drops:' },
  { unicode: '☔️', markdown: ':umbrella:' },
  { unicode: '☂️', markdown: ':open_umbrella:' },
  { unicode: '🌊', markdown: ':ocean:' },
  { unicode: '🌫', markdown: ':fog:' },
]
