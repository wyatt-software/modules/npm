import { useRouter } from 'vue-router'
import { useStore } from 'vuex'
import mitt from 'mitt'

import ArrayExtensions from './lib/array-extensions.js'

import ApiPlugin from './plugins/api-plugin.js'
import ConfirmPromptPlugin from './plugins/confirm-prompt-plugin.js'
import EventBusPlugin from './plugins/event-bus-plugin.js'
import MarkdownPlugin from './plugins/markdown-plugin.js'
import MomentPlugin from './plugins/moment-plugin.js'
import NotificationPlugin from './plugins/notification-plugin.js'
import SignalrPlugin from './plugins/signalr-plugin.js'
import VFormPlugin from './plugins/vform-plugin.js'

export default {
  install(app, options) {
    this.apiBaseUrl = options?.apiBaseUrl || 'http://localhost:5000/'
    this.storageBaseUrl = options?.storageBaseUrl || 'http://localhost:5000/storage/'
    this.recaptchaPublicKey = options?.recaptchaPublicKey || ''

    ArrayExtensions.register()
    var eventBus = mitt()

    app.use(EventBusPlugin, { eventBus })

    app.use(ApiPlugin, { apiBaseUrl: this.apiBaseUrl, storageBaseUrl: this.storageBaseUrl })
    app.use(ConfirmPromptPlugin, { eventBus })
    app.use(MarkdownPlugin, { storageBaseUrl: this.storageBaseUrl })
    app.use(MomentPlugin)
    app.use(NotificationPlugin)
    app.use(SignalrPlugin, { apiBaseUrl: this.apiBaseUrl, eventBus })
    app.use(VFormPlugin, { recaptchaPublicKey: this.recaptchaPublicKey })
  },

  setup() {
    const router = useRouter()
    const store = useStore()

    router.beforeEach((to, from) => {
      // Reset.
      store.dispatch('core/clearViewMeta')

      // Remove any stale meta tags from the document using the key attribute we set below.
      Array.from(document.querySelectorAll('[data-ws]')).map((el) => el.parentNode.removeChild(el))

      // This goes through the matched routes from last to first, finding the closest route with a title.
      // e.g., if we have /some/deep/nested/route and /some, /deep, and /nested have titles,
      // /nested's will be chosen.
      const nearestWithTitle = to.matched
        .slice()
        .reverse()
        .find((r) => r.meta?.title)

      // Find the nearest route element with meta tags.
      const nearestWithMeta = to.matched
        .slice()
        .reverse()
        .find((r) => r.meta?.metaTags)

      const previousNearestWithMeta = from.matched
        .slice()
        .reverse()
        .find((r) => r.meta?.metaTags)

      // If a route with a title was found, set the document (page) title to that value.
      if (nearestWithTitle) {
        document.title = nearestWithTitle.meta.title
        store.dispatch('core/setTitle', nearestWithTitle.meta.title)
        store.dispatch('core/setIconClass', nearestWithTitle.meta.iconClass)
      } else if (previousNearestWithMeta) {
        document.title = previousNearestWithMeta.meta.title
        store.dispatch('core/setTitle', nearestWithTitle.meta.title)
        store.dispatch('core/setIconClass', nearestWithTitle.meta.iconClass)
      }

      // Skip rendering meta tags if there are none.
      if (!nearestWithMeta) {
        return
      }

      // Turn the meta tag definitions into actual elements in the head.
      nearestWithMeta.meta.metaTags
        .map((tagDef) => {
          const tag = document.createElement('meta')

          Object.keys(tagDef).forEach((key) => {
            tag.setAttribute(key, tagDef[key])
          })

          // We use this to track which meta tags we create so we don't interfere with other ones.
          tag.setAttribute('data-ws', '')

          return tag
        })
        // Add the meta tags to the document head.
        .forEach((tag) => document.head.appendChild(tag))
    })
  },
}
