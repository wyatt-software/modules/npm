const state = () => ({
  relativeDatetimes: true,
  manualTimezone: false,
  timezone: '',
})

const getters = {
  relativeDatetimes(state) {
    return state.relativeDatetimes
  },
  manualTimezone(state) {
    return state.manualTimezone
  },
  timezone(state) {
    return state.timezone
  },
}

const mutations = {
  setRelativeDatetimes(state, value) {
    state.relativeDatetimes = value
  },
  setManualTimezone(state, value) {
    state.manualTimezone = value
  },
  setTimezone(state, value) {
    state.timezone = value
  },
}

const actions = {
  setRelativeDatetimes({ commit }, value) {
    commit('setRelativeDatetimes', value)
  },
  setManualTimezone({ commit }, value) {
    commit('setManualTimezone', value)
  },
  setTimezone({ commit }, value) {
    commit('setTimezone', value)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
