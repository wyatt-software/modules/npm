import { RouteHelper } from '../core'

import ContentPagesView from './views/pages/pages-view.vue'
import ContentPageViewView from './views/page-view/page-view-view.vue'
import ContentPageEditView from './views/page-edit/page-edit-view.vue'
import ContentLoadingView from './views/loading/loading-view.vue'

const components = [ContentPagesView, ContentPageViewView, ContentPageEditView, ContentLoadingView]

const meta = [
  {
    id: null,
    sortOrder: 1,
    name: 'content-pages',
    parent: null,
    path: '/pages',
    title: 'Pages',
    menuItem: false,
    iconClass: 'bi bi-book',
    component: 'ContentPagesView',
    redirect: null,
    requiresAuthentication: true,
    requiredRoles: '3',
  },
  {
    id: null,
    sortOrder: 1,
    name: 'content-page-create',
    parent: 'content-pages',
    path: '/create-page',
    title: 'Create Page',
    menuItem: false,
    iconClass: 'bi bi-pen',
    component: 'ContentPageEditView',
    redirect: null,
    requiresAuthentication: true,
    requiredRoles: '3',
  },
  {
    id: null,
    sortOrder: 1,
    name: 'content-page-view',
    parent: 'content-pages',
    path: '/pages/:pageId',
    title: 'View Page',
    menuItem: false,
    iconClass: 'bi bi-book',
    component: 'ContentPageViewView',
    redirect: null,
    requiresAuthentication: false,
    requiredRoles: '',
  },
  {
    id: null,
    sortOrder: 1,
    name: 'content-page-edit',
    parent: 'content-page-view',
    path: '/pages/:pageId/edit',
    title: 'Edit Page',
    menuItem: false,
    iconClass: 'bi bi-pen',
    component: 'ContentPageEditView',
    redirect: null,
    requiresAuthentication: true,
    requiredRoles: '3',
  },

  // We use this special route to catch all unmatched routes.
  // It will show a loading spinner while the content module setup() method fetches the dynamic routes via an API call.
  // When we've got them, we'll remove this route and re-trigger the router.
  {
    id: null,
    sortOrder: 1,
    name: 'content-loading',
    parent: null,
    path: '/:pathMatch(.*)*',
    title: '',
    menuItem: false,
    iconClass: '',
    component: 'ContentLoadingView',
    redirect: null,
    requiresAuthentication: false,
    requiredRoles: '',
  },
]

export default meta.map((x) => RouteHelper.metaToRoute(x, components))
