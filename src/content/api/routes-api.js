export default class RoutesApi {
  constructor(api) {
    this.api = api
    this.controllerBase = `${this.api.baseUrl}content/routes`
  }

  async fetchAsync() {
    return await this.api.http.get(this.controllerBase)
  }
}
