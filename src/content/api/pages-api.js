export default class PagesApi {
  constructor(api, auth) {
    this.api = api
    this.auth = auth
    this.controllerBase = `${this.api.baseUrl}content/pages`
  }

  async createAsync(request) {
    // const request = {
    //   parentId               : 1, // Root = 1
    //   alias                  : 'test',
    //   name                   : 'Test page',
    //   iconsClass             : 'bi bi-image',
    //   component              : 'ContentPage',
    //   redirect               : '',
    //   menuItem               : false,
    //   content                : 'Well **hello** there!',
    //   requiresAuthentication : false,
    // }
    return await this.api.http.post(this.controllerBase, request, { headers: this.auth.getAuthHeaders() })
  }

  async readAsync(id) {
    return await this.api.http.get(`${this.controllerBase}/${id}`)
  }

  async updateAsync(id, request) {
    // const request = {
    //   parentId               : 1, // Root = 1
    //   alias                  : 'test',
    //   name                   : 'Test page',
    //   iconsClass             : 'bi bi-image',
    //   component              : 'ContentPage',
    //   redirect               : '',
    //   menuItem               : false,
    //   content                : 'Well **hello** there!',
    //   requiresAuthentication : false,
    // }
    return await this.api.http.put(`${this.controllerBase}/${id}`, request, { headers: this.auth.getAuthHeaders() })
  }

  async deleteAsync(id) {
    return await this.api.http.delete(`${this.controllerBase}/${id}`, { headers: this.auth.getAuthHeaders() })
  }
}
