const state = () => ({
  pathMatch: '',
  routesLoaded: false,
})

const getters = {
  pathMatch(state) {
    return state.pathMatch
  },
  routesLoaded(state) {
    return state.routesLoaded
  },
}

const mutations = {
  setPathMatch(state, value) {
    state.pathMatch = value
  },
  setRoutesLoaded(state, value) {
    state.routesLoaded = value
  },
}

const actions = {
  setPathMatch({ commit }, value) {
    commit('setPathMatch', value)
  },
  setRoutesLoaded({ commit }, value) {
    commit('setRoutesLoaded', value)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
