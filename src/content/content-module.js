import { onMounted } from 'vue'
import { useRouter, useRoute } from 'vue-router'
import { useStore } from 'vuex'
import { RouteHelper, CoreMenuView, useApi, useNotification } from '../core'
import { useAuth } from '../identity'
import RoutesApi from './api/routes-api.js'
import ContentPageViewView from './views/page-view/page-view-view.vue'

const components = [ContentPageViewView, CoreMenuView]

export default {
  install() {},

  setup() {
    const route = useRoute()
    const router = useRouter()
    const store = useStore()
    const api = useApi()
    const auth = useAuth()
    const notify = useNotification()
    const routesApi = new RoutesApi(api, auth)

    onMounted(async () => {
      let response
      try {
        response = await routesApi.fetchAsync()
      } catch (ex) {
        notify.error('There was a problem getting the page routes.', ex)
        return
      }

      const pathMatch = store.state.content.pathMatch

      let match = null
      response.data.forEach((meta) => {
        const r = RouteHelper.metaToRoute(meta, components)
        router.addRoute(r)
        if (r.path === pathMatch) {
          match = r
        }
      })

      store.dispatch('content/setRoutesLoaded', true)

      router.removeRoute('content-loading')

      if (match) {
        router.replace(match)
      } else if (route.name === 'content-loading') {
        router.push({ name: '404' })
      }
    })
  },
}
