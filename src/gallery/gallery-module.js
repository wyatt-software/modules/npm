import VueLongPressDirective from 'vue-long-press-directive'
import { useVForm } from '../core'
import { useUserAddOn } from '../identity'
import VFormGalleryImages from './components/vform/vform-gallery-images.vue'
import UserAvatarProfileAddOn from './components/user-avatar-profile-add-on.vue'
import UserAvatarListItemAddOn from './components/user-avatar-list-item-add-on.vue'
import UserGalleryButtonAddOn from './components/user-gallery-button-add-on.vue'

import '@fancyapps/ui/dist/fancybox/fancybox.css'

export default {
  install(app) {
    // tap and hold.
    app.use(VueLongPressDirective, { duration: 700 })
  },

  setup() {
    const vform = useVForm()
    const userAddOn = useUserAddOn()

    vform.components['galleryimages'] = VFormGalleryImages

    userAddOn.profileComponents.unshift(UserAvatarProfileAddOn)
    userAddOn.profileTableRows.push(UserGalleryButtonAddOn)

    userAddOn.formFields.unshift({
      field: {
        type: 'galleryimages',
        name: 'imageId',
        label: 'Avatar',
        multiselect: false,
      },
      getter: (user) => user.properties?.imageId,
      setter: (request, value) => {
        request.properties = request.properties || {}
        request.properties.imageId = value
        return request
      },
    })

    // Remove the standard Username component.
    userAddOn.listItemComponents.splice(
      userAddOn.listItemComponents.findIndex((x) => x.name === 'UserUsernameAddOn'),
      1
    )

    userAddOn.listItemComponents.unshift(UserAvatarListItemAddOn)
  },
}
