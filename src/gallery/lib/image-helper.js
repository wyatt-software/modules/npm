export default class ImageHelper {
  static getImageUrl(storageBaseUrl, image, size) {
    const uuid = image.uuid
    const subFolder = uuid.substring(0, 2)
    const extension = image.extension
    const filename = uuid + extension
    return `${storageBaseUrl}gallery/${size}/${subFolder}/${filename}`
  }

  static toMarkdown(storageBaseUrl, images, asThumb) {
    if (!images || images.length === 0) {
      return ''
    }

    return images
      .map((image) => {
        const thumbSrc = ImageHelper.getImageUrl(storageBaseUrl, image, 'thumb')
        const largeSrc = ImageHelper.getImageUrl(storageBaseUrl, image, 'large')
        const originalSrc = ImageHelper.getImageUrl(storageBaseUrl, image, 'original')
        const caption = image.caption
        return asThumb ? `[![${caption}](${thumbSrc})](${originalSrc})` : `![${caption}](${largeSrc})`
      })
      .join(' ')
  }
}
