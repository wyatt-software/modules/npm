// Provides a way for users to manange and select images from their gallery, then return to the content they were
// editing upon selection.
//
// This module represents the return navigation data (stored in global state to avoid extremely long query strings).

const state = () => ({
  returnMode: '',
  returnRoute: null,
  formName: '',
  selectedField: '',
  multiSelect: false,
  selectedImages: [],
})

const getters = {
  returnMode(state) {
    return state.returnMode
  },
  returnRoute(state) {
    return state.returnRoute
  },
  formName(state) {
    return state.formName
  },
  selectedField(state) {
    return state.selectedField
  },
  multiSelect(state) {
    return state.multiSelect
  },
  selectedImages(state) {
    return state.selectedImages
  },
}

const mutations = {
  setReturnMode(state, value) {
    state.returnMode = value
  },
  setReturnRoute(state, value) {
    state.returnRoute = value
  },
  setFormName(state, value) {
    state.formName = value
  },
  setSelectedField(state, value) {
    state.selectedField = value
  },
  setSelectedImages(state, value) {
    state.selectedImages = value
  },
  setMultiSelect(state, value) {
    state.multiSelect = value
  },
  clearImageSelector(state) {
    state.returnMode = ''
    state.returnRoute = {}
    state.formName = ''
    state.selectedField = ''
    state.multiSelect = false
    state.selectedImages = []
  },
}

const actions = {
  setReturnMode({ commit }, value) {
    commit('setReturnMode', value)
  },
  setReturnRoute({ commit }, value) {
    commit('setReturnRoute', value)
  },
  setFormName({ commit }, value) {
    commit('setFormName', value)
  },
  setSelectedField({ commit }, value) {
    commit('setSelectedField', value)
  },
  setSelectedImages({ commit }, value) {
    commit('setSelectedImages', value)
  },
  setMultiSelect({ commit }, value) {
    commit('setMultiSelect', value)
  },
  clearImageSelector({ commit }) {
    commit('clearImageSelector')
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
