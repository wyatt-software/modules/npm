export default class BatchApi {
  constructor(api, auth) {
    this.api = api
    this.auth = auth
    this.controllerBase = `${this.api.baseUrl}gallery/batch`
  }

  async moveImagesAsync(request) {
    // const request = {
    //   imageIds : [4, 5, 6],
    //   albumId  : 123,
    // }
    return await this.api.http.post(`${this.controllerBase}/move-images`, request, {
      headers: this.auth.getAuthHeaders(),
    })
  }

  async deleteImagesAsync(request) {
    // const request = {
    //   imageIds : [4, 5, 6],
    // }
    return await this.api.http.post(`${this.controllerBase}/delete-images`, request, {
      headers: this.auth.getAuthHeaders(),
    })
  }
}
