import { UrlHelper } from '../../core'

export default class AlbumsApi {
  constructor(api, auth) {
    this.api = api
    this.auth = auth
    this.controllerBase = `${this.api.baseUrl}gallery/albums`
  }

  async fetchAsync(params) {
    // const params = {
    //   username : '',
    //   alias    : '',
    //   expand   : '', // [all|image|user]
    //   offset   : 0,
    //   limit    : 20,
    // }

    const url = UrlHelper.getUrl(this.controllerBase, params)
    return await this.api.http.get(url)
  }

  async createAsync(request) {
    // const request = {
    //   displayName: 'whatever',
    //   description: '',
    // }
    return await this.api.http.post(this.controllerBase, request, { headers: this.auth.getAuthHeaders() })
  }

  async readAsync(id, params) {
    // const params = {
    //   expand: '', // [all|image|user]
    // }
    const url = UrlHelper.getUrl(`${this.controllerBase}/${id}`, params)
    return await this.api.http.get(url)
  }

  async updateAsync(id, request) {
    // const request = {
    //   displayName: 'whatever'
    // }
    return await this.api.http.put(`${this.controllerBase}/${id}`, request, { headers: this.auth.getAuthHeaders() })
  }

  async deleteAsync(id) {
    return await this.api.http.delete(`${this.controllerBase}/${id}`, { headers: this.auth.getAuthHeaders() })
  }
}
