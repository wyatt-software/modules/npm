import { UrlHelper } from '../../core'

export default class ImagesApi {
  constructor(api, auth) {
    this.api = api
    this.auth = auth
    this.controllerBase = `${this.api.baseUrl}gallery/images`
  }

  async fetchAsync(params) {
    // const params = {
    //   imageIds : [],
    //   album  : 123, // id (or alias in conjunction with username param)
    //   username : '',
    //   expand: '', // [all|user|album]
    //   offset   : 0,
    //   limit    : 20,
    // }

    const resource = UrlHelper.getUrl(this.controllerBase, params)
    return await this.api.http.get(resource)
  }

  async readAsync(id, params) {
    // const params = {
    //   expand: '', // [all|user|album]
    // }
    const url = UrlHelper.getUrl(`${this.controllerBase}/${id}`, params)
    return await this.api.http.get(url)
  }

  async updateAsync(id, request) {
    // const request = {
    //   filename  : 'sunny.jpg',
    //   caption   : 'Look at my dog!',
    // }
    return await this.api.http.put(`${this.controllerBase}/${id}`, request, { headers: this.auth.getAuthHeaders() })
  }

  async deleteAsync(id) {
    return await this.api.http.delete(`${this.controllerBase}/${id}`, { headers: this.auth.getAuthHeaders() })
  }

  async setAsAvatarAsync(id) {
    return await this.api.http.post(
      `${this.controllerBase}/${id}/actions/set-as-avatar`,
      {},
      { headers: this.auth.getAuthHeaders() }
    )
  }

  async setAsCoverAsync(id) {
    return await this.api.http.post(
      `${this.controllerBase}/${id}/actions/set-as-cover`,
      {},
      { headers: this.auth.getAuthHeaders() }
    )
  }

  // Avatar image upload URL (not an endpoint)
  // This is needed for the DropZone component.
  avatarUploadUrl() {
    const params = {
      albumId: 0,
      setAsAvatar: true,
    }

    return UrlHelper.getUrl(this.controllerBase, params)
  }

  // Image upload URL (not an endpoint)
  // This is needed for the DropZone component.
  uploadUrl(albumId) {
    const params = {
      albumId,
    }

    return UrlHelper.getUrl(this.controllerBase, params)
  }
}
