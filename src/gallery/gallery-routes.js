import { RouteHelper } from '../core'

import GalleryGalleryView from './views/gallery/gallery-view.vue'
import GalleryAlbumsView from './views/albums/albums-view.vue'
import GalleryAlbumEditView from './views/album-edit/album-edit-view.vue'
import GalleryImagesView from './views/images/images-view.vue'
import GalleryImageEditView from './views/image-edit/image-edit-view.vue'

const components = [
  GalleryGalleryView,
  GalleryAlbumsView,
  GalleryAlbumEditView,
  GalleryImagesView,
  GalleryImageEditView,
]

const meta = [
  {
    id: null,
    sortOrder: 1,
    name: 'gallery',
    parent: 'identity-account',
    path: '/gallery',
    title: 'My Gallery',
    menuItem: true,
    iconClass: 'bi bi-images',
    component: 'GalleryGalleryView',
    requiresAuthentication: true,
    requiredRoles: '1',
  },
  {
    id: null,
    sortOrder: 1,
    name: 'gallery-albums',
    parent: 'identity-user-profile',
    path: '/gallery/:username',
    title: 'Gallery',
    menuItem: false,
    iconClass: 'bi bi-images',
    component: 'GalleryAlbumsView',
    redirect: null,
    requiresAuthentication: false,
    requiredRoles: '',
  },
  {
    id: null,
    sortOrder: 1,
    name: 'gallery-album-create',
    parent: 'gallery-albums',
    path: '/gallery/:username/create-album',
    title: 'Create Album',
    menuItem: false,
    iconClass: 'bi bi-pencil-fill',
    component: 'GalleryAlbumEditView',
    redirect: null,
    requiresAuthentication: true,
    requiredRoles: '1',
  },
  {
    id: null,
    sortOrder: 1,
    name: 'gallery-images',
    parent: 'gallery-albums',
    path: '/gallery/:username/albums/:alias',
    title: 'View Album',
    menuItem: false,
    iconClass: 'bi bi-images',
    component: 'GalleryImagesView',
    redirect: null,
    requiresAuthentication: false,
    requiredRoles: '',
  },
  {
    id: null,
    sortOrder: 1,
    name: 'gallery-album-edit',
    parent: 'gallery-images',
    path: '/gallery/:username/albums/:alias/edit',
    title: 'Edit Album',
    menuItem: false,
    iconClass: 'bi bi-pencil-fill',
    component: 'GalleryAlbumEditView',
    redirect: null,
    requiresAuthentication: true,
    requiredRoles: '1',
  },
  {
    id: null,
    sortOrder: 1,
    name: 'gallery-image-edit',
    parent: 'gallery-images',
    path: '/gallery/:username/images/:imageId/edit',
    title: 'Edit Image',
    menuItem: false,
    iconClass: 'bi bi-pencil-fill',
    component: 'GalleryImageEditView',
    redirect: null,
    requiresAuthentication: true,
    requiredRoles: '1',
  },
]

export default meta.map((x) => RouteHelper.metaToRoute(x, components))
