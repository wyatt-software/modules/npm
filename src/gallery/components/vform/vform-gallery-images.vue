<template>
  <div class="vform-gallery-images mb-3">
    <label :for="fieldPrefix + field.name" v-if="field.label">
      {{ field.label }}
      <RequiredIcon v-if="isRequired" />
      <InfoTooltip v-if="field.tooltip" :text="field.tooltip" />
    </label>
    <LoadingSpinner v-if="loading" />
    <div v-if="loaded && none" class="none">
      <ImageUpload
        :url="uploadUrl"
        :allowMultiple="false"
        @image-uploaded="onImageUploaded"
        @queue-complete="onQueueComplete"
      />
    </div>
    <div v-if="loaded && !none" class="thumbs">
      <div v-for="image in images" :key="image.id" class="thumb">
        <img :src="imageSrc(image)" :alt="image.caption" />
      </div>
    </div>
    <div v-if="!field.disabled">
      <button class="btn btn-primary me-2" type="button" @click="onSelectClick">
        <i aria-hidden="true" :class="`bi bi-image${s}`"></i>
        Select...
      </button>
      <button v-if="!none" class="btn btn-primary" type="button" @click="onClearClick">
        <i aria-hidden="true" class="bi bi-backspace"></i>
        Clear
      </button>
    </div>
  </div>
</template>

<script>
import { ref, computed, onMounted } from 'vue'
import { useRouter, useRoute } from 'vue-router'
import { useStore } from 'vuex'
import {
  ObjectHelper,
  InfoTooltip,
  LoadingSpinner,
  RequiredIcon,
  useApi,
  useNotification,
  useVFormControl,
} from '../../../core'
import { useAuth } from '../../../identity'
import ImagesApi from '../../api/images-api.js'
import ImageHelper from '../../lib/image-helper.js'
import ImageUpload from '../../components/image-upload.vue'

export default {
  name: 'VFormGalleryImages',
  emits: ['fieldInput', 'saveState', 'loadState'],
  components: {
    InfoTooltip,
    LoadingSpinner,
    RequiredIcon,
    ImageUpload,
  },
  props: {
    formName: { type: String, required: true },
    field: { type: Object, required: true },
    fieldPrefix: { type: String, required: true },
    modelState: { type: Object, default: null }, // Taken from the API response if validation fails at the server.
    busy: { type: Boolean, default: false },
  },
  setup(props, context) {
    const baseProperties = useVFormControl(props, context)

    const route = useRoute()
    const router = useRouter()
    const store = useStore()
    const api = useApi()
    const auth = useAuth()
    const notify = useNotification()
    const imagesApi = new ImagesApi(api, auth)

    const images = ref([])
    const loading = ref(false)
    const loaded = ref(false)

    const none = computed(() => {
      return !props.field.value
    })

    const s = computed(() => {
      return props.field.multiselect ? 's' : ''
    })

    const uploadUrl = computed(() => {
      return imagesApi.avatarUploadUrl()
    })

    const value = computed(() => {
      return props.field.multiSelect ? images.value.map((x) => x.id).join() : images.value[0]?.id ?? null
    })

    const onImageUploaded = (image) => {
      if (!props.field.multiSelect) {
        images.value = []
      }
      images.value.unshift(image)
    }

    const onQueueComplete = () => {
      const mutatedField = JSON.parse(JSON.stringify(props.field))
      mutatedField.value = value.value
      context.emit('fieldInput', mutatedField)
    }

    const onSelectClick = () => {
      context.emit('saveState', props.field)

      store.dispatch('gallery/setReturnMode', '')
      store.dispatch('gallery/setReturnRoute', {
        name: route.name,
        params: route.params,
        query: route.query,
      })
      store.dispatch('gallery/setFormName', props.formName)
      store.dispatch('gallery/setSelectedField', props.field.name)
      store.dispatch('gallery/setMultiSelect', props.field.multiSelect || false)
      store.dispatch('gallery/setSelectedImages', [])

      router.push({
        name: 'gallery-albums',
        params: { username: store.state.identity.session.username },
        query: {
          // We manage "selecting" mode using a query param rather than via global state so that the workflow is
          // cancelled if the user navigates away from the gallery.
          selecting: true,
        },
      })
    }

    const onClearClick = () => {
      const mutatedField = JSON.parse(JSON.stringify(props.field))
      mutatedField.value = null
      context.emit('fieldInput', mutatedField)
    }

    const imageSrc = (image) => {
      return ImageHelper.getImageUrl(api.storageBaseUrl, image, 'thumb')
    }

    const loadAsync = async () => {
      if (!props.field.value) {
        loading.value = false
        loaded.value = true
        return
      }

      loading.value = true
      loaded.value = false

      try {
        const response = await imagesApi.fetchAsync({
          imageIds: props.field.value,
          expand: 'all',
          offset: 0,
          limit: 0, // Don't paginate
        })

        images.value = props.field.multiselect ? response.data : response.data.slice(0, 1)
        loaded.value = true
      } catch (ex) {
        notify.error('There was a problem fetching the images.', ex)
      } finally {
        loading.value = false
      }
    }

    onMounted(async () => {
      const imageSelector = store.state.gallery

      const returningFromImageSelector =
        route.name === imageSelector.returnRoute?.name &&
        ObjectHelper.equals(route.query, imageSelector.returnRoute?.query) &&
        ObjectHelper.equals(route.params, imageSelector.returnRoute?.params) &&
        props.formName === imageSelector.formName &&
        props.field.name === imageSelector.selectedField

      if (returningFromImageSelector) {
        context.emit('loadState', props.field)

        if (imageSelector.returnMode === 'select') {
          images.value = imageSelector.selectedImages
          store.dispatch('gallery/clearImageSelector')
          loading.value = false
          loaded.value = true

          const mutatedField = JSON.parse(JSON.stringify(props.field))
          mutatedField.value = value.value
          context.emit('fieldInput', mutatedField)
          return
        }
      }

      await loadAsync()
    })

    return {
      ...baseProperties,
      images,
      loading,
      loaded,
      none,
      s,
      uploadUrl,
      value,
      onImageUploaded,
      onQueueComplete,
      onSelectClick,
      onClearClick,
      imageSrc,
    }
  },
}
</script>

<style lang="scss" scoped>
@use '../../../core/sass/vars';

.none {
  margin-bottom: vars.$spacing;
}

.thumbs {
  display: flex;
  flex-wrap: wrap;
  gap: vars.$gallerySpacing;
  margin-bottom: vars.$gallerySpacing;
}

.thumb {
  min-width: vars.$galleryThumbWidth;
}

.thumb img {
  width: 100%;
  height: auto;
}

// Larger displays
@media only screen and (min-width: vars.$sm) {
  .thumbs {
    gap: vars.$gallerySpacingLarge;
    margin-bottom: vars.$gallerySpacingLarge;
  }

  .thumb {
    min-width: vars.$galleryThumbWidthLarge;
  }
}
</style>
