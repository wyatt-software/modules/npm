import SearchModule from './search-module.js'
import SearchRoutes from './search-routes.js'

export { SearchModule, SearchRoutes }
