import { UrlHelper } from '../../core'

export default class SiteSearchApi {
  constructor(api, auth) {
    this.api = api
    this.auth = auth
    this.controllerBase = `${this.api.baseUrl}search`
  }

  async searchAsync(searchIndexName, params) {
    // const params = {
    //   q       : '',
    //   offset  : 0,
    //   limit   : 20
    // }
    const url = UrlHelper.getUrl(`${this.controllerBase}/${searchIndexName}`, params)
    return await this.api.http.get(url)
  }

  async rebuildAsync(searchIndexName) {
    return await this.api.http.post(`${this.controllerBase}/${searchIndexName}/actions/rebuild`, {
      headers: this.auth.getAuthHeaders(),
    })
  }

  getRebuildUrl(searchIndexName) {
    return `${this.controllerBase}/${searchIndexName}/actions/rebuild`
  }
}
