import { RouteHelper } from '../core'

import SearchView from './views/search/search-view.vue'
import SearchRebuildView from './views/rebuild/rebuild-view.vue'

const components = [SearchView, SearchRebuildView]

const meta = [
  {
    id: 0,
    sortOrder: 1,
    name: 'search',
    parent: null,
    path: '/search',
    title: 'Search',
    menuItem: false,
    iconClass: 'bi bi-search',
    component: 'SearchView',
    requiresAuthentication: false,
    requiredRoles: '',
  },
  {
    id: 0,
    sortOrder: 1,
    name: 'search-rebuild',
    parent: 'identity-admin',
    path: '/search/rebuild',
    title: 'Rebuild Search Index',
    menuItem: true,
    iconClass: 'bi bi-search',
    component: 'SearchRebuildView',
    requiresAuthentication: true,
    requiredRoles: '3',
  },
]

export default meta.map((x) => RouteHelper.metaToRoute(x, components))
