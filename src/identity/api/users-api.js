import { UrlHelper } from '../../core'

export default class UsersApi {
  constructor(api) {
    this.api = api
    this.controllerBase = `${this.api.baseUrl}identity/users`
  }

  async metadataAsync(params) {
    // const params = {
    //   q: '',
    // }
    const url = UrlHelper.getUrl(this.controllerBase, params)
    return await this.api.http.head(url)
  }

  async fetchAsync(params) {
    // const params = {
    //   q       : '',
    //   expand: '' // [all|image]
    //   offset  : 0,
    //   limit   : 20,
    //   sort    : '-LastLoggedOn' // [created|lastloggedon|username|location|postcount]
    // }
    const url = UrlHelper.getUrl(this.controllerBase, params)
    return await this.api.http.get(url)
  }

  async createAsync(request) {
    // const request = {
    //   username   : 'sunny',
    //   email      : 'sunny@retrojdm.com',
    //   password   : 'iseekitties',
    //   reCaptcha  : '',
    // }
    return await this.api.http.post(this.controllerBase, request)
  }

  async readAsync(usernameOrEmail, params) {
    // const params = {
    //   expand: '' // [all|image]
    // }
    const url = UrlHelper.getUrl(`${this.controllerBase}/${usernameOrEmail}`, params)
    return await this.api.http.get(url)
  }

  async resendEmailVerificationAsync(usernameOrEmail) {
    return await this.api.http.post(`${this.controllerBase}/${usernameOrEmail}/actions/resend-email-verification`)
  }

  async verifyEmailAsync(usernameOrEmail, request) {
    // const request = {
    //   verificationCode  : '12345678',
    // }
    return await this.api.http.post(`${this.controllerBase}/${usernameOrEmail}/actions/verify-email`, request)
  }

  async forgotPasswordAsync(usernameOrEmail) {
    return await this.api.http.post(`${this.controllerBase}/${usernameOrEmail}/actions/forgot-password`)
  }

  async resetPasswordAsync(usernameOrEmail, request) {
    // const request = {
    //   resetCode    : '12345678',
    //   newPassword  : 'iseesquirrels',
    // }
    return await this.api.http.post(`${this.controllerBase}/${usernameOrEmail}/actions/reset-password`, request)
  }
}
