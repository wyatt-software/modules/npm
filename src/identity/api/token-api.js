import { UrlHelper } from '../../core'

export default class TokenApi {
  constructor(api) {
    this.api = api
    this.controllerBase = `${this.api.baseUrl}connect/token`
  }

  async authenticateAsync(client_id, client_secret, username, password) {
    const resource = this.controllerBase
    const config = { headers: { 'content-type': 'application/x-www-form-urlencoded' } }
    const params = {
      username: username,
      password: password,
      grant_type: 'password',
      client_id: client_id,
      client_secret: client_secret,
    }
    const queryString = UrlHelper.qs(params)

    return await this.api.http.post(resource, queryString, config)
  }

  async refreshAsync(client_id, client_secret, refresh_token) {
    const resource = this.controllerBase
    const config = { headers: { 'content-type': 'application/x-www-form-urlencoded' } }
    const params = {
      refresh_token: refresh_token,
      grant_type: 'refresh_token',
      client_id: client_id,
      client_secret: client_secret,
    }

    const queryString = UrlHelper.qs(params)

    return await this.api.http.post(resource, queryString, config)
  }
}
