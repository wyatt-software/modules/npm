import { UrlHelper } from '../../core'

export default class ValidationApi {
  constructor(api) {
    this.api = api
    this.controllerBase = `${this.api.baseUrl}identity/validation`
  }

  async validateEmailAsync(params) {
    // const params = {
    //   value  : '',
    //   userId : 0
    // }
    const url = UrlHelper.getUrl(`${this.controllerBase}/email`, params)
    return await this.api.http.get(url)
  }

  async validateUsernameAsync(params) {
    // const params = {
    //   value  : '',
    //   userId : 0
    // }
    const url = UrlHelper.getUrl(`${this.controllerBase}/username`, params)
    return await this.api.http.get(url)
  }
}
