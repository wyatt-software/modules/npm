export default class TaskApi {
  constructor(api, auth) {
    this.api = api
    this.auth = auth
    this.controllerBase = `${this.api.baseUrl}core/tasks`
  }

  async fetchAsync() {
    const resource = this.controllerBase
    return await this.api.http.get(resource, { headers: this.auth.getAuthHeaders() })
  }

  async readAsync(key) {
    const resource = `${this.controllerBase}/${key}`
    return await this.api.http.get(resource, { headers: this.auth.getAuthHeaders() })
  }

  async cancelAsync(key) {
    const resource = `${this.controllerBase}/${key}/actions/cancel`
    return await this.api.http.post(resource, {}, { headers: this.auth.getAuthHeaders() })
  }
}
