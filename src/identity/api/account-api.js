import { UrlHelper } from '../../core'

export default class AccountApi {
  constructor(api, auth) {
    this.api = api
    this.auth = auth
    this.controllerBase = `${this.api.baseUrl}identity/account`
  }

  async readAsync(params) {
    // const params = {
    //   expand: '' // [all|image]
    // }
    const url = UrlHelper.getUrl(this.controllerBase, params)
    return await this.api.http.get(url, { headers: this.auth.getAuthHeaders() })
  }

  async updateAsync(request) {
    // const request = {
    //   email: 'sunny@retrojdm.com',
    //   properties: {
    //     location   : 'Roblin, Manitoba, Canada',
    //     signature  : 'Stinkin\' up the place.',
    //   },
    //   preferences: {
    //     emailNewPosts    : true,
    //     emailNewMessages : true,
    //   }
    // }
    return await this.api.http.put(this.controllerBase, request, { headers: this.auth.getAuthHeaders() })
  }

  async changePasswordAsync(request) {
    // const request = {
    //   currentPassword : 'password1',
    //   newPassword     : 'password2'
    // }
    return await this.api.http.post(`${this.controllerBase}/actions/change-password`, request, {
      headers: this.auth.getAuthHeaders(),
    })
  }
}
