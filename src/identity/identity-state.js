import identityStateSession from './identity-state-session.js'

const state = () => ({
  errorDescription: '',
  authenticating: false,
})

const getters = {
  errorDescription(state) {
    return state.errorDescription
  },
  authenticating(state) {
    return state.authenticating
  },
}

const mutations = {
  setErrorDescription(state, value) {
    state.errorDescription = value
  },
  setAuthenticating(state, value) {
    state.authenticating = value
  },
}

const actions = {
  setErrorDescription({ commit }, value) {
    commit('setErrorDescription', value)
  },
  setAuthenticating({ commit }, value) {
    commit('setAuthenticating', value)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
  modules: {
    session: identityStateSession,
  },
}
