import { useStore } from 'vuex'
import { useApi, useVForm, useMoment } from '../core'
import ValidationApi from './api/validation-api.js'
import AuthPlugin, { useAuth } from './plugins/auth-plugin.js'
import UserAddOnPlugin, { useUserAddOn } from './plugins/user-add-on-plugin.js'
import UserProfileTableAddOn from './components/user-profile-table-add-on.vue'
import UserJoinedAddOn from './components/user-joined-add-on.vue'
import UserLastLoginAddOn from './components/user-last-login-add-on.vue'
import UserUsernameAddOn from './components/user-username-add-on.vue'
import UserListItemTableAddOn from './components/user-list-item-table-add-on.vue'

export default {
  install(app, options) {
    app.use(AuthPlugin, {
      clientId: options.clientId,
      clientSecret: options.clientSecret,
    })

    app.use(UserAddOnPlugin)
  },

  setup() {
    const store = useStore()
    const api = useApi()
    const auth = useAuth()
    const moment = useMoment()
    const userAddOn = useUserAddOn()
    const vform = useVForm()
    const validationApi = new ValidationApi(api, auth)

    // Set up timezones.
    if (!store.state.core.persisted.manualTimezone) {
      store.dispatch('core/persisted/setTimezone', moment.tz.guess())
    }

    moment.tz.setDefault(this.timezone)

    // Checks if the given email address is unique (ignoring the currently authenticated user's email).
    vform.validationRules['uniqueemail'] = async (value) => {
      if (!value) {
        // Not really valid, but we don't try to validate unless there's a value.
        // This should'nt be a problem, since we're always pairing this rule with "required".
        return true
      }
      const response = await validationApi.validateEmailAsync({
        value: value,
        userId: store.state.identity.session.userId,
      })
      return response.data.valid || response.data.data.message
    }

    vform.validationRules['uniqueusername'] = async (value) => {
      if (!value) {
        // Not really valid, but we don't try to validate unless there's a value.
        // This should'nt be a problem, since we're always pairing this rule with "required".
        return true
      }
      const response = await validationApi.validateUsernameAsync({
        value: value,
        userId: store.state.identity.session.userId,
      })
      return response.data.valid || response.data.data.message
    }

    // We register components etc. to the "User Add-on" plugin like any other component.
    // This keeps the logic simpler and allows customisation (i.e.: removing default parts).
    userAddOn.profileComponents.push(UserProfileTableAddOn)
    userAddOn.profileTableRows.push(UserJoinedAddOn)
    userAddOn.profileTableRows.push(UserLastLoginAddOn)

    userAddOn.formFields.push({
      field: {
        type: 'email',
        name: 'email',
        label: 'Email',
        maxlength: 255,
        validate: 'required|email|uniqueemail',
      },
      getter: (user) => user?.email,
      setter: (request, value) => {
        request.email = value
        return request
      },
    })

    userAddOn.filters.push({ text: 'Date joined ', value: 'created' })
    userAddOn.filters.push({ text: 'Date last logged on', value: 'lastloggedon' })
    userAddOn.filters.push({ text: 'Username ', value: ' Sort by username ' })

    userAddOn.listItemComponents.push(UserUsernameAddOn)
    userAddOn.listItemComponents.push(UserListItemTableAddOn)
    userAddOn.listItemTableRows.push(UserJoinedAddOn)
    userAddOn.listItemTableRows.push(UserLastLoginAddOn)

    auth.setup()
  },
}
