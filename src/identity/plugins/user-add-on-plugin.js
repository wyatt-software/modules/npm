import { inject } from 'vue'

const UserAddOnSymbol = Symbol()

/**
 * A plugin for adding functionaily to the user.
 */
class UserAddOnPlugin {
  constructor() {
    // Profile Components - Used in "UserProfile". Eg:
    //
    //   userAddOn.profileComponents.push(SomeCustomComponent)
    //
    this.profileComponents = []

    // Profile Table - Used in "UserProfile". Eg:
    //
    //   userAddOn.profileTableRows.push(SomeCustomComponent)
    //
    this.profileTableRows = []

    // Form Fields - Used in "UserProfileTable" (which is the default profile component) Eg:
    //
    //   userAddOn.formFields.push({
    //     field: {
    //       type: 'text',
    //       name: 'whatever',
    //       label: 'Whatever'
    //     },
    //     getter: (user) => user.preferences.whatever,
    //     setter: (request, value) => {
    //       request.preferences = request.preferences || {}
    //       request.preferences.whatever = value
    //       return request
    //     }
    //   })
    //
    this.formFields = []

    // Filters - Used in "UserListfilter". Eg:
    //
    //   userAddOn.filters.push({ text: 'Sort by something', value: 'something' }})
    //
    this.filters = []

    // list Item Components - Used in "UserListItem". Eg:
    //
    //   userAddOn.listItemComponents.push(SomeCustomComponent)
    //
    this.listItemComponents = []

    // List Item Table Rows - Used in "UserListItemTable" (which is the default list item component) Eg:
    //
    //   userAddOn.listItemTableRows.push(SomeCustomComponent)
    //
    this.listItemTableRows = []
  }
}

export default {
  install(app) {
    app.provide(UserAddOnSymbol, new UserAddOnPlugin())
  },
}

export function useUserAddOn() {
  const userAddOn = inject(UserAddOnSymbol)
  if (!userAddOn) {
    throw "The userAddOn plugin hasn't been installed in your app."
  }
  return userAddOn
}
