const SILENT_REFRESH_SECONDS_BEFORE_ACCESS_TOKEN_EXPIRES = 120
const SILENT_REFRESH_SECONDS_CHECK_INTERVAL = 60

import { inject, onMounted } from 'vue'
import { useStore } from 'vuex'
import { useRouter } from 'vue-router'
import { StringHelper, useApi, useNotification } from '../../core'
import TokenApi from '../api/token-api.js'

const AuthSymbol = Symbol()

/**
 * A plugin for authentication and silent refreshes.
 */
class AuthPlugin {
  constructor(clientId, clientSecret) {
    this.clientId = clientId
    this.clientSecret = clientSecret

    this.store = null
    this.router = null
    this.api = null
    this.notify = null
    this.tokenApi = null
  }

  /**
   * Register the app instance and clientId with the custom OAuth plugin.
   * @param {object} app
   * @param {string} clientId
   * @param {string} clientSecret
   */
  setup() {
    this.store = useStore()
    this.router = useRouter()
    this.api = useApi()
    this.notify = useNotification()
    this.tokenApi = new TokenApi(this.api)

    const ctx = this

    onMounted(async () => {
      await ctx.checkRefreshAsync()
    })

    // Check once now, then periodically if we need to refresh the access token.
    setInterval(async () => {
      await ctx.checkRefreshAsync()
    }, SILENT_REFRESH_SECONDS_CHECK_INTERVAL * 1000)
  }

  /**
   * Gets an object containing the Bearer token with the currently authenticated user's JWT.
   */
  getAuthHeaders() {
    const accessToken = this.store.state.identity.session.accessToken
    return {
      Authorization: `Bearer ${accessToken}`,
    }
  }

  /**
   * Try to authenticate with a username/password pair.
   * @param {string} username
   * @param {string} password
   */
  async authenticateAsync(username, password) {
    this.store.dispatch('identity/session/logout')
    this.store.dispatch('identity/setErrorDescription', '')
    this.store.dispatch('identity/setAuthenticating', true)

    try {
      const response = await this.tokenApi.authenticateAsync(this.clientId, this.clientSecret, username, password)
      this.success(response)
      this.notify.success('Successfully logged on')
    } catch (ex) {
      this.failure(ex)
    }
  }

  /**
   * Reqests a refresh token if the access token will be expiring soon.
   */
  async checkRefreshAsync() {
    const expires = new Date(this.store.state.identity.session.accessTokenExpires)
    if (!expires) {
      if (this.store.state.identity.session.accessToken) {
        this.store.dispatch('identity/session/logout')
      }
      return
    }

    const expiresIn = parseInt((new Date(expires).getTime() - new Date().getTime()) / 1000)

    if (expiresIn < SILENT_REFRESH_SECONDS_BEFORE_ACCESS_TOKEN_EXPIRES) {
      await this.refreshAsync()
    }
  }

  /**
   * Requests a new access token with the refresh token stored in memory.
   */
  async refreshAsync() {
    this.store.dispatch('identity/setErrorDescription', '')
    this.store.dispatch('identity/setAuthenticating', true)
    const refresh_token = this.store.state.identity.session.refreshToken

    try {
      const response = await this.tokenApi.refreshAsync(this.clientId, this.clientSecret, refresh_token)
      this.success(response)
    } catch (ex) {
      this.failure(ex)
    }
  }

  /**
   * General success callback. Used by both authenticate and refresh.
   * @param {object} response
   */
  success(response) {
    const expires = new Date()
    expires.setSeconds(expires.getSeconds() + response.data.expires_in)

    this.store.dispatch('identity/session/setAccessToken', response.data.access_token)
    this.store.dispatch('identity/session/setAccessTokenExpires', expires.toISOString())
    this.store.dispatch('identity/session/setRefreshToken', response.data.refresh_token)
    this.store.dispatch('identity/setErrorDescription', '')

    // Get the decoded JWT payload.
    const access_token = response.data.access_token
    const payload = JSON.parse(atob(access_token.split('.')[1]))

    // Jam the single role into an array if it isn't already.
    const roles = Array.isArray(payload.role) ? payload.role : StringHelper.isString(payload.role) ? [] : [payload.role]

    this.store.dispatch('identity/session/setUserId', parseInt(payload.sub))
    this.store.dispatch('identity/session/setRoles', roles)
    this.store.dispatch('identity/session/setUsername', payload.name)
    this.store.dispatch('identity/setAuthenticating', false)
    this.store.dispatch('messaging/setUnreadMessageCount', parseInt(payload.messages))
  }

  /**
   * General failure callback (used by both authenitcation and refresh).
   * @param {object} response
   */
  failure(ex) {
    if (
      ex?.response?.data?.error === 'invalid_grant' &&
      ex?.response?.data?.error_description?.includes('not verified')
    ) {
      this.store.dispatch('identity/setErrorDescription', ex.response.data.error_description)
      this.store.dispatch('identity/setAuthenticating', false)
      this.router.push({ name: 'identity-public-email-verification' })
      return
    }

    if (ex?.response?.data?.error_description) {
      // Authentication failed
      this.store.dispatch('identity/setErrorDescription', ex.response.data.error_description)
    } else {
      // Server error
      this.notify.error('There was a problem with the authentication server.', ex)
    }
    this.store.dispatch('identity/session/logout')
    this.store.dispatch('identity/setAuthenticating', false)
  }
}

export default {
  install(app, options) {
    const clientId = options?.clientId || ''
    const clientSecret = options?.clientSecret || ''

    app.provide(AuthSymbol, new AuthPlugin(clientId, clientSecret))
  },
}

export function useAuth() {
  const auth = inject(AuthSymbol)
  if (!auth) {
    throw "The Auth plugin hasn't been installed in your app."
  }
  return auth
}
