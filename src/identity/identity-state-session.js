// NOTE ON SECURITY
// It's not secure to store a refresh token on the client side.
// Unfortunately, this means the user will have to re-authenticate whenever they
// open a new session.
//
// We can't use and HttpOnly cookie, because the IdentityServer4 doesn't support it, and customisation of their token
// response via middleware is a nightmare.
//
// DO NOT STORE ON CLIENT SIDE
// This should only be in-memory, and lost with the browser session.
//
const state = () => ({
  accessToken: '',
  accessTokenExpires: '', // ISO date string
  refreshToken: '',
  userId: 0,
  roles: [],
  username: '',
})

const getters = {
  accessToken(state) {
    return state.accessToken
  },
  accessTokenExpires(state) {
    return state.accessTokenExpires
  },
  refreshToken(state) {
    return state.refreshToken
  },
  userId(state) {
    return state.userId
  },
  roles(state) {
    return state.roles
  },
  username(state) {
    return state.username
  },
}

const mutations = {
  setAccessToken(state, value) {
    state.accessToken = value
  },
  setAccessTokenExpires(state, value) {
    state.accessTokenExpires = value
  },
  setRefreshToken(state, value) {
    state.refreshToken = value
  },
  setUserId(state, value) {
    state.userId = value
  },
  setRoles(state, value) {
    state.roles = JSON.parse(JSON.stringify(value))
  },
  setUsername(state, value) {
    state.username = value
  },
  logout(state) {
    state.accessToken = ''
    state.accessTokenExpires = ''
    state.refreshToken = ''
    state.userId = 0
    state.roles = []
    state.username = ''
  },
}

const actions = {
  setAccessToken({ commit }, value) {
    commit('setAccessToken', value)
  },
  setAccessTokenExpires({ commit }, value) {
    commit('setAccessTokenExpires', value)
  },
  setRefreshToken({ commit }, value) {
    commit('setRefreshToken', value)
  },
  setUserId({ commit }, value) {
    commit('setUserId', value)
  },
  setRoles({ commit }, value) {
    commit('setRoles', value)
  },
  setUsername({ commit }, value) {
    commit('setUsername', value)
  },
  logout({ commit }) {
    commit('logout')
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
