/ [Home](../../README.md) / Set up your local dev environment

# Set up your local dev environment

This guide assumes you're using Windows 10 and developing in Google Chrome.

## Install

### git

Download and install [git for windows](https://gitforwindows.org/).

### Node.js

Download and install [Node.js](https://nodejs.org/en/).

### Vue CLI

Download and install [Vue CLI](https://cli.vuejs.org/).

### Visual Studio Code (optional)

Download and install [Visual Studio Code](https://code.visualstudio.com/download).

### Vue.js devtools (optional)

Download and the install [Vue.js devtools](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd?hl=en) Chrome browser extension.

### prettier and eslint (optional)

You may want to also follow this guide for setting VSCode up for Vue.js with prettier and eslint:
https://mattgosden.medium.com/quick-vscode-setup-for-vue-js-with-prettier-and-eslint-4b97fc71c587

### NPM Link

Take a look at the scripts in the root [README](../../README.md) for info on linking/unlinking local packages for local development.
