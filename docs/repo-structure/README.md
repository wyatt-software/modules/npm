/ [Home](../../README.md) / Repo structure

# Repo structure

- `docs` - Documentation common to all modules
- `{module}` - Each module follows the same structure
  - `dist/` - build output folder
  - `docs/` - documentation
  - `node_modules/` - NPM packages/dependencies
  - `src/` - Vue.js source
    - `api/` - Server API access
    - `components/` - SFCs (.vue files)
    - `lib/` - static helper functions
    - `plugins/` - Add global-level functionality
    - `sass/` - SASS CSS
    - `views/` - page level SFCs
    - `{module}-module.js` - A Vue.js plug that can be "used". Also includes a `setup()` function to be called in the main app's `setup()` hook
    - `{module}-routes.js` - The hard-coded routes to the views (pages) for this module
    - `{module}-state-persisted.js` - Any state that needs to be persisted in local storage
    - `{module}-state-session.js` - Any state that needs to be persisted in the browser session
    - `{module}-state.js` - Any state that is not persisted (resides in momory, and is lost when the browser is closed)
    - `index.js` - Entry-point for the NPM package.
  - `.jsconfig` - specify which browsers should be supported
  - `package-lock.json` - Lock dependency versions
  - `package.json` - Dependency management and script definitions
  - `README.md` - Entry-point for module-specific docs
  - `vue.config.js` - Vue.js configuration
- `.gitignore` - Define files/folders excluded from source-conrol
- `build-all.sh` - See root [README](../../README.md) for info on bash scripts.
- `link-all.sh`
- `publish-all.sh`
- `unlink-all.sh`
- `unpublish-all.sh`
- `README.md` - Overview of the entire repo
- `TODO.md` - Pending bugfixes and features still yet to implement
