# Wyatt Software Vue.js 3.x Modules

## About

This repo contains the source code for a group of modules that form the "building blocks" of a Vue.js 3.x Single Page Application (SPA).

They're intended to be included in your project as a git sub-module:

```bash
cd <your-repo-root>
git submodule add https://gitlab.com/wyatt-software/modules/client-web-modules
```

An SPA built with these modules can be used as the client-side part of a web application:

![Web Application Diagram](./docs/images/app-diagram-web-client.png)

| Repo                                                             | Description                                            |
| ---------------------------------------------------------------- | ------------------------------------------------------ |
| [client-web-modules](https://gitlab.com/wyatt-software/modules/client-web-modules) (this repo) | Source code for Vue.js 3.x sub-module                 |
| [server-modules](https://gitlab.com/wyatt-software/modules/server-modules)                     | Source code for `WyattSoftware.Modules` .NET projects |

## Module dependencies

You can choose to add only the modules you need, but certain modules depend on others.

![Module dependencies](./docs/images/module-dependencies.png)

The `Identity` and `Gallery` modules are special cases that allow specific parts of their functionality to be extended with add-ons implemented in other modules. This allows things like User avatars to only be implemented when the `Gallery` module is installed, or for a Topic in the `Forum` module to be updated when it's thumbnail image is deleted.

\* _The "Add-ons" relationship from the `Forum` module to the `Gallery` module is not shown in this diagram._

## Documentation

### Common to all modules

- [Set up your local dev environment](./docs/local-development/README.md)
- [Repo structure](./docs/repo-structure/README.md)

### Modules

- [core](./core/README.md)
- [identity](./identity/README.md)
- [search](./search/README.md)
- [content](./content/README.md)
- [gallery](./gallery/README.md)
- [messaging](./messaging/README.md)
- [forum](./forum/README.md)
