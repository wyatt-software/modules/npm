# TODO

- Nitpick code. Eg:

  - Convert to Composition API

    - return only what is used in the template
    - check internal/intermediate values - decide if we need `ref`, `reactive`, `computed`, `const`, or `let`
    - re-visit `confirm` and `action-button` logic. i.e.: accessing methods from parent!?
    - emit page meta data instead of using vuex?

  - merge RouteHelper and ApiPlugin?
  - Test pagination - especially in search results
  - Check uses of `JSON.parse(JSON.stringify(...))`
  - Check all `watch` logic. Consider:

    - moving to `updated()` hook,
    - using navigation guards, or
    - using `:key` to refresh a component

- Nitpick style. Eg:

  - Remove CSS overrides, and instead use CSS variables to change bootstrap colours
  - Implement light and dark modes via CSS variables

- Code splitting (webpack dynamic imports? / async components? / lazy-loaded routes?)

  - `initial`

    - Ensure we're minimizing code
    - GZip bundles/assets
    - include only the bare minimum app layout (header/footer/sidebar)
    - Use placeholder components. i.e.: blank grey rectangles for entire sections
    - We want to get the initial bundle below the recommended 244kb
    - Lazy-load vendor dependencies like `vue`, `vue-router`, `vuex` and `bootstrap`

  - `public`

    - Load only the components needed for public users
    - Lazy-load vendor dependencies like `axios`, and `moment`, `fancybox` etc.
    - We want the lowest possible "time to interactive"

  - `members`

    - Any part of the site hidden behind a login wall
    - Lazy-load vendor dependencies like `toast-ui`, `signalr`, `dropzone`, `vue-long-press-directive` etc.

  - `admin`
    - Further split admin features, so that regular members don't load them unnecessarily
    - For security (and to keep the public app simpler), possibly create a completely separate admin app / domain.

- Core

  - Markdown editor: Toast UI editor v3.0

- Identity

  - Admin area for Email template management (part of the identity module because it relies on Auth plugin)
  - Encrypt password reset code and email verification code in DB
  - Use an Http-Only cookie. See https://medium.com/@sadnub/simple-and-secure-api-authentication-for-spas-e46bcea592ad

- Content

  - Admin area for Page management

- Gallery

  - Convert Dropzone to "Upload" button in VForm component
  - edit profile

    - style avatar with `border-radius: 50%;`
    - save avatar independent of rest of form?

  - Integrate with Toast UI Editor (popup/plugin)
  - Improve the upload UI (i.e.: fix error handling + show a loading spinner)

- Forum
  - Admin area for Category/Section management
  - Automatic topic thumbs
